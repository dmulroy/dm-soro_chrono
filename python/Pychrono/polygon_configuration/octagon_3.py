# PyChrono script generated from SolidWorks using Chrono::SolidWorks add-in 
# Assembly: 


import pychrono as chrono 
import builtins 

shapes_dir = 'octagon_3_shapes/' 

if hasattr(builtins, 'exported_system_relpath'): 
    shapes_dir = builtins.exported_system_relpath + shapes_dir 

exported_items = [] 

body_0= chrono.ChBodyAuxRef()
body_0.SetName('ground')
body_0.SetBodyFixed(True)
exported_items.append(body_0)

# Rigid body part
body_1= chrono.ChBodyAuxRef()
body_1.SetName('octagon2-1')
body_1.SetPos(chrono.ChVectorD(0.323223304614268,0,0.0517766953857323))
body_1.SetRot(chrono.ChQuaternionD(1,0,0,0))
body_1.SetMass(5.17766953488278)
body_1.SetInertiaXX(chrono.ChVectorD(0.0224606554635367,0.0427639486208723,0.0224606554635367))
body_1.SetInertiaXY(chrono.ChVectorD(0,0,0))
body_1.SetFrame_COG_to_REF(chrono.ChFrameD(chrono.ChVectorD(0,0.025,0),chrono.ChQuaternionD(1,0,0,0)))

# Visualization shape 
body_1_1_shape = chrono.ChObjShapeFile() 
body_1_1_shape.SetFilename(shapes_dir +'body_1_1.obj') 
body_1_1_level = chrono.ChAssetLevel() 
body_1_1_level.GetFrame().SetPos(chrono.ChVectorD(0,0,0)) 
body_1_1_level.GetFrame().SetRot(chrono.ChQuaternionD(1,0,0,0)) 
body_1_1_level.GetAssets().push_back(body_1_1_shape) 
#########################################################################
octagon2_texture = chrono.ChTexture()
octagon2_texture.SetTextureFilename('.\data\octB.png')
body_1_1_level.GetAssets().push_back(octagon2_texture)
#########################################################################
body_1.GetAssets().push_back(body_1_1_level) 

# Collision shapes 
body_1.GetCollisionModel().ClearModel()
pt_vect = chrono.vector_ChVectorD()
pt_vect.push_back(chrono.ChVectorD(0.0517766953227324,0.05,0.125000000063))
pt_vect.push_back(chrono.ChVectorD(-0.0517766953227323,0.05,0.125000000063))
pt_vect.push_back(chrono.ChVectorD(-0.125000000063,0.05,0.0517766953227323))
pt_vect.push_back(chrono.ChVectorD(-0.125000000063,0.05,-0.0517766953227323))
pt_vect.push_back(chrono.ChVectorD(-0.0517766953227323,0.05,-0.125000000063))
pt_vect.push_back(chrono.ChVectorD(0.0517766953227323,0.05,-0.125000000063))
pt_vect.push_back(chrono.ChVectorD(0.125000000063,0.05,-0.0517766953227323))
pt_vect.push_back(chrono.ChVectorD(0.125000000063,0.05,0.0517766953227323))
pt_vect.push_back(chrono.ChVectorD(0.0517766953227324,0,0.125000000063))
pt_vect.push_back(chrono.ChVectorD(-0.0517766953227323,0,0.125000000063))
pt_vect.push_back(chrono.ChVectorD(-0.125000000063,0,0.0517766953227323))
pt_vect.push_back(chrono.ChVectorD(-0.125000000063,0,-0.0517766953227323))
pt_vect.push_back(chrono.ChVectorD(-0.0517766953227323,0,-0.125000000063))
pt_vect.push_back(chrono.ChVectorD(0.0517766953227323,0,-0.125000000063))
pt_vect.push_back(chrono.ChVectorD(0.125000000063,0,-0.0517766953227323))
pt_vect.push_back(chrono.ChVectorD(0.125000000063,0,0.0517766953227323))
body_1.GetCollisionModel().AddConvexHull(pt_vect)
body_1.GetCollisionModel().BuildModel()
body_1.SetCollide(True)

exported_items.append(body_1)



# Rigid body part
body_2= chrono.ChBodyAuxRef()
body_2.SetName('octagon1-1')
body_2.SetPos(chrono.ChVectorD(0.5,0,-0.125))
body_2.SetRot(chrono.ChQuaternionD(1,0,0,0))
body_2.SetMass(5.17766953488278)
body_2.SetInertiaXX(chrono.ChVectorD(0.0224606554635367,0.0427639486208723,0.0224606554635367))
body_2.SetInertiaXY(chrono.ChVectorD(0,0,0))
body_2.SetFrame_COG_to_REF(chrono.ChFrameD(chrono.ChVectorD(0,0.025,0),chrono.ChQuaternionD(1,0,0,0)))

# Visualization shape 
body_2_1_shape = chrono.ChObjShapeFile() 
body_2_1_shape.SetFilename(shapes_dir +'body_2_1.obj') 
body_2_1_level = chrono.ChAssetLevel() 
body_2_1_level.GetFrame().SetPos(chrono.ChVectorD(0,0,0)) 
body_2_1_level.GetFrame().SetRot(chrono.ChQuaternionD(1,0,0,0)) 
body_2_1_level.GetAssets().push_back(body_2_1_shape) 
#########################################################################
octagon1_texture = chrono.ChTexture()
octagon1_texture.SetTextureFilename('.\data\octA.png')
body_2_1_level.GetAssets().push_back(octagon1_texture)
#########################################################################
body_2.GetAssets().push_back(body_2_1_level) 

# Collision shapes 
body_2.GetCollisionModel().ClearModel()
pt_vect = chrono.vector_ChVectorD()
pt_vect.push_back(chrono.ChVectorD(0.0517766953227324,0.05,0.125000000063))
pt_vect.push_back(chrono.ChVectorD(-0.0517766953227323,0.05,0.125000000063))
pt_vect.push_back(chrono.ChVectorD(-0.125000000063,0.05,0.0517766953227323))
pt_vect.push_back(chrono.ChVectorD(-0.125000000063,0.05,-0.0517766953227323))
pt_vect.push_back(chrono.ChVectorD(-0.0517766953227323,0.05,-0.125000000063))
pt_vect.push_back(chrono.ChVectorD(0.0517766953227323,0.05,-0.125000000063))
pt_vect.push_back(chrono.ChVectorD(0.125000000063,0.05,-0.0517766953227323))
pt_vect.push_back(chrono.ChVectorD(0.125000000063,0.05,0.0517766953227323))
pt_vect.push_back(chrono.ChVectorD(0.0517766953227324,0,0.125000000063))
pt_vect.push_back(chrono.ChVectorD(-0.0517766953227323,0,0.125000000063))
pt_vect.push_back(chrono.ChVectorD(-0.125000000063,0,0.0517766953227323))
pt_vect.push_back(chrono.ChVectorD(-0.125000000063,0,-0.0517766953227323))
pt_vect.push_back(chrono.ChVectorD(-0.0517766953227323,0,-0.125000000063))
pt_vect.push_back(chrono.ChVectorD(0.0517766953227323,0,-0.125000000063))
pt_vect.push_back(chrono.ChVectorD(0.125000000063,0,-0.0517766953227323))
pt_vect.push_back(chrono.ChVectorD(0.125000000063,0,0.0517766953227323))
body_2.GetCollisionModel().AddConvexHull(pt_vect)
body_2.GetCollisionModel().BuildModel()
body_2.SetCollide(True)

exported_items.append(body_2)



# Rigid body part
body_3= chrono.ChBodyAuxRef()
body_3.SetName('ground-1')
body_3.SetPos(chrono.ChVectorD(0,0,0))
body_3.SetRot(chrono.ChQuaternionD(1,0,0,0))
body_3.SetMass(799.999999184)
body_3.SetInertiaXX(chrono.ChVectorD(267.333333059293,533.333332789333,267.333333059293))
body_3.SetInertiaXY(chrono.ChVectorD(0,3.40468394022094e-14,0))
body_3.SetFrame_COG_to_REF(chrono.ChFrameD(chrono.ChVectorD(0,-0.049999999949,0),chrono.ChQuaternionD(1,0,0,0)))

# Visualization shape 
body_3_1_shape = chrono.ChObjShapeFile() 
body_3_1_shape.SetFilename(shapes_dir +'body_3_1.obj') 
body_3_1_level = chrono.ChAssetLevel() 
body_3_1_level.GetFrame().SetPos(chrono.ChVectorD(0,0,0)) 
body_3_1_level.GetFrame().SetRot(chrono.ChQuaternionD(1,0,0,0)) 
body_3_1_level.GetAssets().push_back(body_3_1_shape) 

#########################################################################
ground_texture = chrono.ChTexture()
ground_texture.SetTextureFilename('.\data\ground.png')
body_3_1_level.GetAssets().push_back(ground_texture)
#########################################################################

body_3.GetAssets().push_back(body_3_1_level) 

# Collision shapes 
body_3.GetCollisionModel().ClearModel()
mr = chrono.ChMatrix33D()
mr[0,0]=1; mr[1,0]=0; mr[2,0]=-5.55111512312578E-17 
mr[0,1]=0; mr[1,1]=0; mr[2,1]=1 
mr[0,2]=0; mr[1,2]=-1; mr[2,2]=0 
body_3.GetCollisionModel().AddBox(1,1,0.049999999949,chrono.ChVectorD(0,-0.049999999949,2.22044604925031E-16),mr)
body_3.GetCollisionModel().BuildModel()
body_3.SetCollide(True)

exported_items.append(body_3)




# Mate constraint: Coincident1 [MateCoincident] type:0 align:0 flip:False
#   Entity 0: C::E name: body_3 , SW name: ground-1 ,  SW ref.type:4 (4)
#   Entity 1: C::E name:  , SW name: Assem3 ,  SW ref.type:4 (4)

link_1 = chrono.ChLinkMateXdistance()
cA = chrono.ChVectorD(0,0,0)
cB = chrono.ChVectorD(0,0,0)
dA = chrono.ChVectorD(0,0,1)
dB = chrono.ChVectorD(0,0,1)
link_1.Initialize(body_3,body_0,False,cA,cB,dB)
link_1.SetDistance(0)
link_1.SetName("Coincident1")
exported_items.append(link_1)

link_2 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(0,0,0)
dA = chrono.ChVectorD(0,0,1)
cB = chrono.ChVectorD(0,0,0)
dB = chrono.ChVectorD(0,0,1)
link_2.Initialize(body_3,body_0,False,cA,cB,dA,dB)
link_2.SetName("Coincident1")
exported_items.append(link_2)


# Mate constraint: Coincident2 [MateCoincident] type:0 align:0 flip:False
#   Entity 0: C::E name: body_3 , SW name: ground-1 ,  SW ref.type:4 (4)
#   Entity 1: C::E name:  , SW name: Assem3 ,  SW ref.type:4 (4)

link_3 = chrono.ChLinkMateXdistance()
cA = chrono.ChVectorD(0,0,0)
cB = chrono.ChVectorD(0,0,0)
dA = chrono.ChVectorD(0,1,0)
dB = chrono.ChVectorD(0,1,0)
link_3.Initialize(body_3,body_0,False,cA,cB,dB)
link_3.SetDistance(0)
link_3.SetName("Coincident2")
exported_items.append(link_3)

link_4 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(0,0,0)
dA = chrono.ChVectorD(0,1,0)
cB = chrono.ChVectorD(0,0,0)
dB = chrono.ChVectorD(0,1,0)
link_4.Initialize(body_3,body_0,False,cA,cB,dA,dB)
link_4.SetName("Coincident2")
exported_items.append(link_4)


# Mate constraint: Coincident3 [MateCoincident] type:0 align:0 flip:False
#   Entity 0: C::E name: body_3 , SW name: ground-1 ,  SW ref.type:4 (4)
#   Entity 1: C::E name:  , SW name: Assem3 ,  SW ref.type:4 (4)

link_5 = chrono.ChLinkMateXdistance()
cA = chrono.ChVectorD(0,0,0)
cB = chrono.ChVectorD(0,0,0)
dA = chrono.ChVectorD(1,0,0)
dB = chrono.ChVectorD(1,0,0)
link_5.Initialize(body_3,body_0,False,cA,cB,dB)
link_5.SetDistance(0)
link_5.SetName("Coincident3")
exported_items.append(link_5)

link_6 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(0,0,0)
dA = chrono.ChVectorD(1,0,0)
cB = chrono.ChVectorD(0,0,0)
dB = chrono.ChVectorD(1,0,0)
link_6.Initialize(body_3,body_0,False,cA,cB,dA,dB)
link_6.SetName("Coincident3")
exported_items.append(link_6)


# Mate constraint: Coincident4 [MateCoincident] type:0 align:0 flip:False
#   Entity 0: C::E name: body_3 , SW name: ground-1 ,  SW ref.type:4 (4)
#   Entity 1: C::E name: body_2 , SW name: octagon1-1 ,  SW ref.type:4 (4)

link_7 = chrono.ChLinkMateXdistance()
cA = chrono.ChVectorD(0,0,0)
cB = chrono.ChVectorD(0.5,0,-0.125)
dA = chrono.ChVectorD(0,1,0)
dB = chrono.ChVectorD(0,1,0)
link_7.Initialize(body_3,body_2,False,cA,cB,dB)
link_7.SetDistance(0)
link_7.SetName("Coincident4")
exported_items.append(link_7)

link_8 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(0,0,0)
dA = chrono.ChVectorD(0,1,0)
cB = chrono.ChVectorD(0.5,0,-0.125)
dB = chrono.ChVectorD(0,1,0)
link_8.Initialize(body_3,body_2,False,cA,cB,dA,dB)
link_8.SetName("Coincident4")
exported_items.append(link_8)


# Mate constraint: Distance1 [MateDistanceDim] type:5 align:0 flip:False
#   Entity 0: C::E name: body_2 , SW name: octagon1-1 ,  SW ref.type:4 (4)
#   Entity 1: C::E name:  , SW name: Assem3 ,  SW ref.type:4 (4)

link_9 = chrono.ChLinkMateXdistance()
cA = chrono.ChVectorD(0.5,0,-0.125)
cB = chrono.ChVectorD(0,0,0)
dA = chrono.ChVectorD(0,0,1)
dB = chrono.ChVectorD(0,0,1)
link_9.Initialize(body_2,body_0,False,cA,cB,dB)
link_9.SetDistance(-0.125)
link_9.SetName("Distance1")
exported_items.append(link_9)

link_10 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(0.5,0,-0.125)
dA = chrono.ChVectorD(0,0,1)
cB = chrono.ChVectorD(0,0,0)
dB = chrono.ChVectorD(0,0,1)
link_10.Initialize(body_2,body_0,False,cA,cB,dA,dB)
link_10.SetName("Distance1")
exported_items.append(link_10)


# Mate constraint: Distance2 [MateDistanceDim] type:5 align:0 flip:False
#   Entity 0: C::E name: body_3 , SW name: ground-1 ,  SW ref.type:4 (4)
#   Entity 1: C::E name: body_2 , SW name: octagon1-1 ,  SW ref.type:4 (4)

link_11 = chrono.ChLinkMateXdistance()
cA = chrono.ChVectorD(0,0,0)
cB = chrono.ChVectorD(0.5,0,-0.125)
dA = chrono.ChVectorD(1,0,0)
dB = chrono.ChVectorD(1,0,0)
link_11.Initialize(body_3,body_2,False,cA,cB,dB)
link_11.SetDistance(-0.5)
link_11.SetName("Distance2")
exported_items.append(link_11)

link_12 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(0,0,0)
dA = chrono.ChVectorD(1,0,0)
cB = chrono.ChVectorD(0.5,0,-0.125)
dB = chrono.ChVectorD(1,0,0)
link_12.Initialize(body_3,body_2,False,cA,cB,dA,dB)
link_12.SetName("Distance2")
exported_items.append(link_12)


# Mate constraint: Coincident7 [MateCoincident] type:0 align:0 flip:False
#   Entity 0: C::E name: body_3 , SW name: ground-1 ,  SW ref.type:4 (4)
#   Entity 1: C::E name: body_1 , SW name: octagon2-1 ,  SW ref.type:4 (4)
"""
link_13 = chrono.ChLinkMateXdistance()
cA = chrono.ChVectorD(0,0,0)
cB = chrono.ChVectorD(0.323223304614268,0,0.0517766953857323)
dA = chrono.ChVectorD(0,1,0)
dB = chrono.ChVectorD(0,1,0)
link_13.Initialize(body_3,body_1,False,cA,cB,dB)
link_13.SetDistance(0)
link_13.SetName("Coincident7")
exported_items.append(link_13)

link_14 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(0,0,0)
dA = chrono.ChVectorD(0,1,0)
cB = chrono.ChVectorD(0.323223304614268,0,0.0517766953857323)
dB = chrono.ChVectorD(0,1,0)
link_14.Initialize(body_3,body_1,False,cA,cB,dA,dB)
link_14.SetName("Coincident7")
exported_items.append(link_14)

"""