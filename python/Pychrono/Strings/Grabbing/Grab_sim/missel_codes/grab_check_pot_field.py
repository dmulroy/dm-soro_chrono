# -*- coding: utf-8 -*-
"""
Created on Tue Jun  2 13:42:44 2020

@author: dmulr
"""


import pychrono.core as chrono
import pychrono.irrlicht as chronoirr
import pychrono.postprocess as postprocess
import os
import numpy as np
import timeit
from grab_sim_shape_objects import *
from config import *
from scipy.spatial import distance
from scipy.interpolate import Rbf
from scipy.interpolate import RegularGridInterpolator


shapes=Points_for_shape(shape,zball,xball,zball2,xball2,nb,diameter,bl,br,R,nr,Rd)
(x,y,z)=shapes.Points_for_shape()
(rbf)=shapes.create_RBF()
shapes.Plot_rbf()
