# -*- coding: utf-8 -*-
"""
Created on Fri Jun  5 17:45:24 2020

@author: dmulr
"""

import numpy as np
import math as math
import matplotlib.pyplot as plt
import os
import numpy as np
import math as math
import matplotlib.pyplot as plt
from matplotlib import animation
import animatplot as amp
from matplotlib import colors as colors
from scipy.spatial import Voronoi, voronoi_plot_2d,ConvexHull
from os import listdir
from grab_sim_shape_plot_objects import *
from config import *

# Set the file path


cases=[12,13,14]
variable=["200","250","300"]
graphs=compare_cases(cases,variable,nb)
#graphs.plot_compare()
graphs.plot_compare_shape()