# -*- coding: utf-8 -*-
"""
Created on Fri Jun  5 17:45:24 2020

@author: dmulr
"""

import numpy as np
import math as math
import matplotlib.pyplot as plt
import os
import numpy as np
import math as math
import matplotlib.pyplot as plt
from matplotlib import animation
import animatplot as amp
from matplotlib import colors as colors
from scipy.spatial import Voronoi, voronoi_plot_2d,ConvexHull
from os import listdir
from config import *

# Set the file path


cs=['r','g','b']
filepath1='C:/Users/dmulr/OneDrive\Documents/dm-soro_chrono/python/Pychrono/Grabbing/Grab_sim_pot_field_shape/robot_data0/'
filepath2='C:/Users/dmulr/OneDrive\Documents/dm-soro_chrono/python/Pychrono/Grabbing/Grab_sim_pot_field_shape/robot_data3/'
filepath3='C:/Users/dmulr/OneDrive\Documents/dm-soro_chrono/python/Pychrono/Grabbing/Grab_sim_pot_field_shape/robot_data6/'

filesx1=filepath1+"X_points_shape.csv"
filesy1=filepath1+"Y_points_shape.csv"
filesx2=filepath2+"X_points_shape.csv"
filesy2=filepath2+"Y_points_shape.csv"
filesx3=filepath3+"X_points_shape.csv"
filesy3=filepath3+"Y_points_shape.csv"
shape=["0-2","3-5","6"]


xd1 = np.genfromtxt(filesx1,delimiter=',')
yd1 = np.genfromtxt(filesy1,delimiter=',')
xd2 = np.genfromtxt(filesx2,delimiter=',')
yd2 = np.genfromtxt(filesy2,delimiter=',')
xd3 = np.genfromtxt(filesx3,delimiter=',')
yd3 = np.genfromtxt(filesy3,delimiter=',')


plt.scatter(xd1,yd1,color=cs[0],label=shape[0])    
plt.scatter(xd2,yd2,color=cs[1],label=shape[1]) 
plt.scatter(xd3,yd3,color=cs[2],label=shape[2]) 
plt.legend()
plt.grid(True)
    
