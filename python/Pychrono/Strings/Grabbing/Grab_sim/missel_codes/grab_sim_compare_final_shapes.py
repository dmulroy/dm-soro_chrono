# -*- coding: utf-8 -*-
"""
Created on Fri Jun  5 17:45:24 2020

@author: dmulr
"""

import numpy as np
import math as math
import matplotlib.pyplot as plt
import os
import numpy as np
import math as math
import matplotlib.pyplot as plt
from matplotlib import animation
import animatplot as amp
from matplotlib import colors as colors
from scipy.spatial import Voronoi, voronoi_plot_2d,ConvexHull
from os import listdir
from config import *

# Set the file path

datax={}
dataz={}
cases=[6,7,8]
for i in cases:
    
    datax["d{0}".format(i)]=[]
    dataz["d{0}".format(i)]=[]
    
cs=['r','g','b']
filepath='C:/Users/dmulr/OneDrive\Documents/dm-soro_chrono/python/Pychrono/Grabbing/Grab_sim_pot_field_shape/robot_data6/'
filesx=filepath+"X_points_shape.csv"
filesy=filepath+"Y_points_shape.csv"
alpha=["30","80","130"]


xd = np.genfromtxt(filesx,delimiter=',')
yd = np.genfromtxt(filesy,delimiter=',')
for i in cases:

    path='C:/Users/dmulr/OneDrive\Documents/dm-soro_chrono/python/Pychrono/Grabbing/Grab_sim_pot_field_shape/robot_data'+str(i)+'/'
    name='bot_position.csv'
    filename=path+name
    data1 = np.genfromtxt(filename,delimiter=',')

    data1=data1
    (m1,n1)=np.shape(data1)
    data1=data1[:,1:n1]

    xpos=data1[1:nb+1,:]
    ypos=data1[nb+1:2*nb+1,:]
    zpos=data1[(2*nb)+1:3*nb+1,:]
    
    zf=zpos[:,-1]
    xf=xpos[:,-1]
    datax["d"+str(i)].append(xf)
    dataz["d"+str(i)].append(zf)


count=0
for i in cases:
    plt.scatter(dataz["d"+str(i)],datax["d"+str(i)],color=cs[count],label="alpha="+alpha[count])
    count=count+1

plt.scatter(xd,yd,color="black",label="desired")    
plt.legend(loc='center left')
plt.grid(True)
    
