# -*- coding: utf-8 -*-
"""
Created on Mon Jun 22 12:33:21 2020

@author: dmulr
"""
import pychrono as chrono 
import builtins 
import os
import math
import time
import sys, getopt
import pychrono as chrono
import pychrono.postprocess as postprocess
import pychrono.irrlicht as chronoirr
import numpy as np
import csv

path='C:/Users/dmulr/OneDrive/Documents/dm-soro_chrono/python/Pychrono/Strings/Grabbing/Grab_sim_pot_field_shape/data/'
path2='C:/Users/dmulr/OneDrive/Documents/dm-soro_chrono/python/Pychrono/Strings/Grabbing/Grab_sim_pot_field_shape/octagon_3_shapes/'


m_visualization = "irrlicht"
m_timestep=.001
mu_f=.1    # friction
mu_b=.01    # dampning
mu_r=.01     # rolling friction
mu_s=.01    # SPinning fiction


##### compliance #####

Ct=.0001
C=.000001
Cr=.0001
Cs=.0001
length=8  # Length of the body floor
tall=1     # height of the body floor

class polygon_ball:
    def __init__(self,my_system,path):
        self.R=.5
        self.nsides=12
        self.my_system=my_system
        self.path=path
        pt_vect = chrono.vector_ChVectorD()
    
        for i in range(self.nsides):
            if i==0 or i==4:
                pt_vect.push_back(chrono.ChVectorD(3*self.R*np.cos(i*2*np.pi/self.nsides),-.125,3*self.R*np.sin(i*2*np.pi/self.nsides)))
            else:
                pt_vect.push_back(chrono.ChVectorD(self.R*np.cos(i*2*np.pi/self.nsides),-.125,self.R*np.sin(i*2*np.pi/self.nsides)))

        for i in range(self.nsides):
            if i==0 or i==4:
                pt_vect.push_back(chrono.ChVectorD(3*self.R*np.cos(i*2*np.pi/self.nsides),.125,3*self.R*np.sin(i*2*np.pi/self.nsides)))
            else:
                pt_vect.push_back(chrono.ChVectorD(self.R*np.cos(i*2*np.pi/self.nsides),.125,self.R*np.sin(i*2*np.pi/self.nsides)))
        
        self.body_1= chrono.ChBodyAuxRef()
        self.body_1.SetFrame_COG_to_REF(chrono.ChFrameD(chrono.ChVectorD(0,0,0),chrono.ChQuaternionD(1,0,0,0)))
        self.body_1=chrono.ChBodyEasyConvexHull(pt_vect,100,True,True)   
        self.body_1.SetPos(chrono.ChVectorD(0,1.5,0.0))
        rotation1 = chrono.ChQuaternionD()
        rotation1.Q_from_AngAxis(np.pi/4, chrono.ChVectorD(1, 0, 0)) 
        self.body_1.SetRot(rotation1)

        
        self.body_1.GetCollisionModel().ClearModel()
        self.body_1.GetCollisionModel().AddConvexHull(pt_vect)
        self.body_1.GetCollisionModel().BuildModel()
        self.body_1.SetCollide(True)
        self.body_1.SetBodyFixed(False)
        self.body_1.SetName('octagon2-1')
        polygon_texture = chrono.ChTexture()
        polygon_texture.SetTextureFilename(self.path+'octB.png')
        self.body_1.GetAssets().push_back(polygon_texture)
        self.my_system.Add(self.body_1)
        
        
class star_ball:
    def __init__(self,my_system,path):
        self.R=.5
        
        self.nsides=35
        self.my_system=my_system
        self.path=path
        pt_vect = chrono.vector_ChVectorD()
    
        for i in range(self.nsides):
            
            if i%2==0:
                R3=self.R
            else:
                R3=2*self.R
            print(R3)    
            pt_vect.push_back(chrono.ChVectorD(R3*np.cos(i*2*np.pi/self.nsides),-.125,R3*np.sin(i*2*np.pi/self.nsides)))
            
        for i in range(self.nsides):
            if i%2==0:
                R3=self.R
            else:
                R3=2*self.R
            pt_vect.push_back(chrono.ChVectorD(R3*np.cos(i*2*np.pi/self.nsides),.125,R3*np.sin(i*2*np.pi/self.nsides)))
            
       
        
        self.body_1= chrono.ChBodyAuxRef()
        self.body_1.SetFrame_COG_to_REF(chrono.ChFrameD(chrono.ChVectorD(0,0,0),chrono.ChQuaternionD(1,0,0,0)))
        self.body_1=chrono.ChBodyEasyConvexHull(pt_vect,100,True,True)   
        self.body_1.SetPos(chrono.ChVectorD(0,.125,0.0))
        rotation1 = chrono.ChQuaternionD()
        rotation1.Q_from_AngAxis(0, chrono.ChVectorD(1, 0, 0)) 
        self.body_1.SetRot(rotation1)

        
        self.body_1.GetCollisionModel().ClearModel()
        self.body_1.GetCollisionModel().AddConvexHull(pt_vect)
        self.body_1.GetCollisionModel().BuildModel()
        self.body_1.SetCollide(True)
        self.body_1.SetBodyFixed(True)
        self.body_1.SetName('octagon2-1')
        polygon_texture = chrono.ChTexture()
        polygon_texture.SetTextureFilename(self.path+'octB.png')
        self.body_1.GetAssets().push_back(polygon_texture)
        self.my_system.Add(self.body_1)


#[Create Floor]
def Floor(material,length,tall):
    body_floor = chrono.ChBody()
    body_floor.SetBodyFixed(True)
    body_floor.SetPos(chrono.ChVectorD(0, -tall, 0 ))
    body_floor.SetMaterialSurface(material)
    body_floor.GetCollisionModel().ClearModel()
    body_floor.GetCollisionModel().AddBox(length, tall, length) # hemi sizes
    body_floor.GetCollisionModel().BuildModel()
    body_floor.SetCollide(True)
    body_floor_shape = chrono.ChBoxShape()
    body_floor_shape.GetBoxGeometry().Size = chrono.ChVectorD(length, tall, length)
    body_floor.GetAssets().push_back(body_floor_shape)
    body_floor_texture = chrono.ChTexture()
    body_floor_texture.SetTextureFilename(chrono.GetChronoDataPath() + 'aluminum.jpg')
    body_floor.GetAssets().push_back(body_floor_texture)
    return(body_floor)
    
def Material(mu_f,mu_b,mu_r,mu_s,C,Ct,Cr,Cs):
    material = chrono.ChMaterialSurfaceNSC()
    material.SetFriction(mu_f)
    material.SetDampingF(mu_b)
    material.SetCompliance (C)
    material.SetComplianceT(Ct)
    material.SetRollingFriction(mu_r)
    material.SetSpinningFriction(mu_s)
    material.SetComplianceRolling(Cr)
    material.SetComplianceSpinning(Cs)
    return material
    
    
chrono.SetChronoDataPath(path2)

my_system = chrono.ChSystemNSC()

#material=Material(mu_f,mu_b,mu_r,mu_s,C,Ct,Cr,Cs)

#(body_floor)=Floor(material,length,tall)
#my_system.Add(body_floor)
#polygon_ball(my_system,path)

# Rigid body part
body_B= chrono.ChBodyAuxRef()
body_B.SetPos(chrono.ChVectorD(0,0.5,0))
body_B.SetMass(16)
body_B.SetInertiaXX(chrono.ChVectorD(0.270,0.400,0.427))
body_B.SetInertiaXY(chrono.ChVectorD(0.057,0.037,-0.062))
body_B.SetFrame_COG_to_REF(chrono.ChFrameD(
            chrono.ChVectorD( 0.12,0.0,0),
            chrono.ChQuaternionD(1,0,0,0)))

# Attach a visualization shape .
# First load a .obj from disk into a ChTriangleMeshConnected:
mesh_for_visualization = chrono.ChTriangleMeshConnected()
mesh_for_visualization.LoadWavefrontMesh(chrono.GetChronoDataFile('star.obj'))
# Optionally: you can scale/shrink/rotate the mesh using this:
#mesh_for_visualization.Transform(chrono.ChVectorD(0.01,0,0), chrono.ChMatrix33D(1))
# Now the  triangle mesh is inserted in a ChTriangleMeshShape visualization asset, 
# and added to the body
visualization_shape = chrono.ChTriangleMeshShape()
visualization_shape.SetMesh(mesh_for_visualization)
body_B.AddAsset(visualization_shape)
polygon_texture = chrono.ChTexture()
polygon_texture.SetTextureFilename(path+'octB.png')
body_B.GetAssets().push_back(polygon_texture)

# Add the collision shape.
# Again load a .obj file in Wavefront file format. NOTE: in this
# example we use the same .obj file as for visualization, but here one
# could do a better thing: using a different low-level-of-detail mesh for the 
# collision, so the simulation performance is not affected by many details such 
# as bolts and chamfers that may be wanted only for visualization.
mesh_for_collision = chrono.ChTriangleMeshConnected()
mesh_for_collision.LoadWavefrontMesh(chrono.GetChronoDataFile('star.obj'))
# Optionally: you can scale/shrink/rotate the mesh using this:
mesh_for_collision.Transform(chrono.ChVectorD(0.01,0,0), chrono.ChMatrix33D(1))
body_B.GetCollisionModel().ClearModel()
body_B.GetCollisionModel().AddTriangleMesh(
            mesh_for_collision, # the mesh 
            False,  # is it static?
            False)  # is it convex?
            # , mpos, mr,  # pos of mesh respect to REF and rotation matr.respect to REF 
            # 0.01) # 'inflating' radiust for triangles for increased robustness
body_B.GetCollisionModel().BuildModel()
body_B.SetCollide(True)
body_B.SetBodyFixed(True)
my_system.Add(body_B)


if m_visualization == "irrlicht":

	#  Create an Irrlicht application to visualize the system

    myapplication = chronoirr.ChIrrApp(my_system, 'Octagon', chronoirr.dimension2du(1280,720))

    myapplication.AddTypicalSky(path + 'skybox/')
    myapplication.AddTypicalCamera(chronoirr.vector3df(1,1,1),chronoirr.vector3df(0.0,0.0,0.0))
    myapplication.AddTypicalLights()
    myapplication.SetSymbolscale(0.002)
    myapplication.SetShowInfos(True)
    myapplication.SetContactsDrawMode(2)
    myapplication.AssetBindAll();
    myapplication.AssetUpdateAll();
    myapplication.SetTimestep(m_timestep);
    myapplication.BeginScene()
    myapplication.DrawAll()
    myapplication.DoStep()
    myapplication.EndScene()
    while(myapplication.GetDevice().run()):
        myapplication.BeginScene()
        myapplication.DrawAll()
        myapplication.DoStep()
        myapplication.EndScene()    
    