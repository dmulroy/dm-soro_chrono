# -*- coding: utf-8 -*-
"""
Created on Tue May 12 12:50:52 2020

@author: dmulr
"""
import numpy as np
import math as math
import matplotlib.pyplot as plt
import os
from config import *
from grab_sim_plot_objects import *
#path='C:/Users/dmulr/OneDrive/Documents/dm-soro_chrono/PyChrono/Phase Diagrams/phase_diagrams2/robot_data'+sim+'/'
path='C:/Users/dmulr/OneDrive/Documents/dm-soro_chrono/python/Pychrono/Strings/Grabbing/Grab_sim_pot_field_shape/robot_data'+sim+'/'

# 0
name='bot_position.csv'
filename=path+name
data0 = np.genfromtxt(filename,delimiter=',')

# 1
name='bot_velocity.csv'
filename=path+name
data1 = np.genfromtxt(filename,delimiter=',')

# 2
name='bot_TotalForces.csv'
filename=path+name
data2 = np.genfromtxt(filename,delimiter=',')

# 3
name='Force_controller.csv'
filename=path+name
data3 = np.genfromtxt(filename,delimiter=',')

# 4
name='Error.csv'
filename=path+name
data4 = np.genfromtxt(filename,delimiter=',')

# 5
name='Spring_properties.csv'
filename=path+name
data5 = np.genfromtxt(filename,delimiter=',')
#################################################################
# 6
name='x contact points.csv'
filename=path+name
data6 = np.genfromtxt(filename,delimiter=',')
# 7
name='y contact points.csv'
filename=path+name
data7 = np.genfromtxt(filename,delimiter=',')

# 8
name='z contact points.csv'
filename=path+name
data8 = np.genfromtxt(filename,delimiter=',')

# 9
name='x contact force.csv'
filename=path+name
data9 = np.genfromtxt(filename,delimiter=',')

# 10
name='y contact force.csv'
filename=path+name
data10 = np.genfromtxt(filename,delimiter=',')

# 11
name='z contact force.csv'
filename=path+name
data11 = np.genfromtxt(filename,delimiter=',')

# 12
name='nc.csv'
filename=path+name
data12 = np.genfromtxt(filename,delimiter=',')
######################################################################
# 13
name="X_points_shape.csv"
filename=path+name
data13 = np.genfromtxt(filename,delimiter=',')

# 14
name="Y_points_shape.csv"
filename=path+name
data14 = np.genfromtxt(filename,delimiter=',')

########################################################################
# 15
name="particle_position.csv"
filename=path+name
data15 = np.genfromtxt(filename,delimiter=',')

## 16
name="particle_velocity.csv"
filename=path+name
data16 = np.genfromtxt(filename,delimiter=',')
############################################################################
# 17
#name="X_points_shape2.csv"
#filename=path+name
#data17 = np.genfromtxt(filename,delimiter=',')
#
## 18
#name="Y_points_shape2.csv"
#filename=path+name
#data18 = np.genfromtxt(filename,delimiter=',')
#######################################################################

## 19
name="ballx.csv"
filename=path+name
data19 = np.genfromtxt(filename,delimiter=',')
  

## 20
name="ballz.csv"
filename=path+name
data20 = np.genfromtxt(filename,delimiter=',')
results_dir = os.path.join('plots'+sim) 



if not os.path.isdir(results_dir):
    os.makedirs(results_dir)
results=robot_plots(data0,data1,data2,data3,data4,data5,data13,data14,data19,data20,nb,results_dir)
results.plot_ball()
#results.plot_xyz()
#results.Plot_fxyz()
#results.Plot_fxyzcontroller()
#results.Plot_spring_force_length()
#results.compare_configuration()
#results.plot_error()
#results.Plot_shape()
#results.Plot_shape_circle()
#results.Control_force_sesmic_x()
#results.Control_force_sesmic_z()
#results.Control_force_sesmic()
#results.Control_force_sesmic_animation()
phi=Packing_fraction(data0,data15,nb,diameter,results_dir)
phi.calculate_packing()
phi.plot_packing()
phi.stat_packing()
#results2=plot_force_chain(data0,data6,data7,data8,data9,data10,data11,data12,results_dir,nb)
#results2.Forcechains()
