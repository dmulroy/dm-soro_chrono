# -*- coding: utf-8 -*-
"""
Created on Tue May 12 12:53:23 2020

@author: dmulr
"""
import numpy as np
import math as math
import matplotlib.pyplot as plt
import os
import numpy as np
import math as math
import matplotlib.pyplot as plt
from matplotlib import colors as colors
import matplotlib as mpl
import os
from matplotlib import animation
import animatplot as amp
import matplotlib.cm as cm
from matplotlib import colors as colors
from scipy.spatial import Voronoi, voronoi_plot_2d,ConvexHull
import statistics 
class robot_plots:
    def __init__(self,data0,data1,data2,data3,data4,data5,data13,data14,data19,data20,nb,results_dir):
        
        # position
        self.data0=data0
        (self.m0,self.n0)=np.shape(self.data0)
        self.data0=self.data0[:,1:self.n0]
        self.time=self.data0[0,:]
        self.xpos=self.data0[1:nb+1,:]
        self.ypos=self.data0[nb+1:2*nb+1,:]
        self.zpos=self.data0[(2*nb)+1:3*nb+1,:]       
        
        # velocity
        self.data1=data1
        (self.m1,self.n1)=np.shape(self.data1)
        self.data1=self.data1[:,1:self.n1]
        self.time=self.data1[0,:]
        self.xvel=self.data1[1:nb+1,:]
        self.yvel=self.data1[nb+1:2*nb+1,:]
        self.zvel=self.data1[(2*nb)+1:3*nb+1,:]
    # total force
        self.data2=data2
        (self.m2,self.n2)=np.shape(self.data2)
        self.data2=self.data2[:,1:self.n2]
        self.Fx=self.data2[1:nb+1,:]
        self.Fy=self.data2[nb+1:2*nb+1,:]
        self.Fz=self.data2[(2*nb)+1:3*nb+1,:]
    
    # controller force
        self.data3=data3
        (self.m3,self.n3)=np.shape(self.data3)
        self.data3=self.data3[:,1:self.n3]
        self.Fcx=self.data3[1:nb+1,:]
        self.Fcy=self.data3[nb+1:2*nb+1,:]
        self.Fcz=self.data3[(2*nb)+1:3*nb+1,:]
    # error
        self.data4=data4
        self.Error=self.data4
        
        # springs
        self.data5=data5
        (self.m5,self.n5)=np.shape(self.data5)
        self.data5=self.data5[:,1:self.n5]
        self.Fsprings=self.data5[1:nb+1,:]
        self.Length_springs=self.data5[nb+1:2*nb+1,:]
        
        # X deisred points
        self.data13=data13
        self.X=self.data13
        # Ydesired points
        self.data14=data14
        self.Y=self.data14
        
#        self.data17=data17
#        self.X2=self.data17
#        
#        self.data18=data18
#        self.Y2=self.data18
        
        self.data19=data19
        self.data20=data20
        self.ballx=self.data19
        self.ballz=self.data20
        
        
        self.nb=nb
        self.results_dir=results_dir

# plot each robots x position
    def plot_xyz(self):
        direct = os.path.join(self.results_dir,'xyz positions')
        
    
        if not os.path.isdir(direct):
            os.makedirs(direct)
        for i in range (self.nb):
            print(i)
            fig=plt.figure(i)
            plt.figure(figsize=(15,10))
    
            fig.suptitle("Position  vs Time (s) for Bot " + str(i))
            ax1 = plt.subplot(3,1,1)
            ax1.grid(True)
            plt.gca().set_title('x position (mm) vs time')
            plt.plot(self.time, self.xpos[i,:]*1000,'b')
    
            ax2 = plt.subplot(3,1,2)
            plt.gca().set_title('y position(mm) vs time')
            plt.plot(self.time,self.ypos[i,:]*1000,'r')
            ax2.grid(True)
            
            ax3 = plt.subplot(3,1,3)
            plt.gca().set_title('z position (mm) vs time')
            plt.plot(self.time, self.zpos[i,:]*1000,'g')
            ax3.grid(True)
            plt.subplots_adjust(hspace = 1)
            plt.xlabel('time (seconds)')
            plt.savefig(direct+'/'+str(i)+".pdf") 
            plt.close('all')
            
            
            
# Plot forces            
    def Plot_fxyz(self):
            
        direct = os.path.join(self.results_dir,'xyz Forces')
        
    
        if not os.path.isdir(direct):
            os.makedirs(direct)
        for i in range (self.nb):
            print(i)
            fig=plt.figure(i)
            plt.figure(figsize=(15,10))
    
            fig.suptitle("Force vs Time (s) for Bot " + str(i))
            ax1 = plt.subplot(3,1,1)
            ax1.grid(True)
            plt.gca().set_title('X Force (N) vs time')
            plt.plot(self.time, self.Fx[i,:],'b')
    
            ax2 = plt.subplot(3,1,2)
            plt.gca().set_title('Y Force (N) vs time')
            plt.plot(self.time,self.Fy[i,:],'r')
            ax2.grid(True)
            
            ax3 = plt.subplot(3,1,3)
            plt.gca().set_title('Z Force (N) vs time')
            plt.plot(self.time, self.Fz[i,:],'g')
            ax3.grid(True)
            plt.subplots_adjust(hspace = 1)
            plt.xlabel('time (seconds)')
            plt.savefig(direct+'/'+str(i)+".pdf") 
            plt.close('all')   
            
# plot controller forces            
    def Plot_fxyzcontroller(self):
            
        direct = os.path.join(self.results_dir,'xyz Controller Forces')
        
    
        if not os.path.isdir(direct):
            os.makedirs(direct)
        for i in range (self.nb):
            print(i)
            fig=plt.figure(i)
            plt.figure(figsize=(15,10))
    
            fig.suptitle("Force vs Time (s) for Bot " + str(i))
            ax1 = plt.subplot(3,1,1)
            ax1.grid(True)
            plt.gca().set_title('X Force (N) vs time')
            plt.plot(self.time, self.Fcx[i,:],'b')
    
            ax2 = plt.subplot(3,1,2)
            plt.gca().set_title('Y Force (N) vs time')
            plt.plot(self.time,self.Fcy[i,:],'r')
            ax2.grid(True)
            
            ax3 = plt.subplot(3,1,3)
            plt.gca().set_title('Z Force (N) vs time')
            plt.plot(self.time, self.Fcz[i,:],'g')
            ax3.grid(True)
            plt.subplots_adjust(hspace = 1)
            plt.xlabel('time (seconds)')
            plt.savefig(direct+'/'+str(i)+".pdf") 
            plt.close('all')       
            
    def plot_error(self):
        direct = os.path.join(self.results_dir,'Error')
        plt.figure(figsize=(15,10))
        plt.plot(self.time,self.Error,'b')
        plt.xlabel('time')
        plt.ylabel('error')
        plt.title('error vs time')
        plt.grid(True)
        plt.savefig(direct+".pdf")  
        plt.close('all')
        

         
# plot spring force and lengths
    def Plot_spring_force_length(self):
                
        direct = os.path.join(self.results_dir,'Spring_force_length')
        
    
        if not os.path.isdir(direct):
            os.makedirs(direct)
        for i in range (self.nb):
            print(i)
            fig=plt.figure(i)
            plt.figure(figsize=(15,10))
            ax1 = plt.subplot(2,1,1)
            ax1.grid(True)
            plt.gca().set_title('length (N) vs time')
            plt.plot(self.time,self.Length_springs[i,:],'b')
    
            ax2 = plt.subplot(2,1,2)
            plt.gca().set_title('Force vs time for bot')
            plt.plot(self.time,self.Fsprings[i,:],'r')
            ax2.grid(True)
            plt.savefig(direct+'/'+str(i)+".pdf") 
            plt.close('all')
            
    def Plot_shape(self):
        direct = os.path.join(self.results_dir)
        if not os.path.isdir(direct):
            os.makedirs(direct)
        fig=plt.figure(1)
        plt.scatter(self.X[0:self.nb-3],self.Y[0:self.nb-3],label=' desired',color='b')
        plt.scatter(self.zpos[:,-1],self.xpos[:,-1],label='actual',color='r')
        plt.grid(True)
        plt.legend()
        plt.savefig(direct+"/shape.pdf") 
        plt.close('all')
        
    def Plot_shape_circle(self):
        direct = os.path.join(self.results_dir)
        if not os.path.isdir(direct):
            os.makedirs(direct)
        fig=plt.figure(1)
        plt.scatter(self.X[0:self.nb],self.Y[0:self.nb],label=' desired',color='b')
        plt.scatter(self.zpos[:,-1],self.xpos[:,-1],label='actual',color='r')
        circle1=plt.Circle((.3,0),.3,color='g')
        plt.gcf().gca().add_artist(circle1)
        plt.grid(True)
        plt.legend()    
        
    def compare_configuration(self):
        direct = os.path.join(self.results_dir)
        if not os.path.isdir(direct):
            os.makedirs(direct)
        plt.scatter(self.X,self.Y,color="blue",label="Desired")
        plt.scatter(self.X2,self.Y2,color="green",label="Desired2")
        plt.scatter(self.zpos[:,-1],self.xpos[:,-1],label='actual',color='r')
        plt.legend(loc='center left')
        plt.grid(True)   
        plt.savefig(direct+"/shape2.pdf") 
        plt.close('all') 
        
    def Control_force_sesmic_x(self):    
    
        results_dir = os.path.join(self.results_dir, 'Total_Force/')     
        if not os.path.isdir(results_dir):
            os.makedirs(results_dir)
        cmap = plt.cm.get_cmap('seismic')
        boundaries=np.arange(np.amin(self.Fcx),np.amax(self.Fcx),.1)
        norm = colors.BoundaryNorm(boundaries, cmap.N, [boundaries[0], 100])


        plt.figure(1,figsize=(8,8),dpi=150)
        F=abs(self.Fcx[:,-1])
        plt.scatter(self.zpos[:,-1],self.xpos[:,-1],s=2*np.power(F,.65),c=F,cmap=cmap,norm=norm)
        plt.xlim(-.75,.75)
        plt.ylim(-.75,.75)
        plt.grid(True)
        plt.colorbar()
        plt.xlabel('z position(m)')
        plt.ylabel('x position(m)')
        
        
    def Control_force_sesmic_z(self):    
    
        results_dir = os.path.join(self.results_dir, 'Total_Force/')     
        if not os.path.isdir(results_dir):
            os.makedirs(results_dir)
   
        cmap = plt.cm.get_cmap('seismic')
    

    
        boundaries=np.arange(np.amin(self.Fcz),np.amax(self.Fcz),.1)
        norm = colors.BoundaryNorm(boundaries, cmap.N, [boundaries[0], 100])


        plt.figure(2,figsize=(8,8),dpi=150)
        F=abs(self.Fcx[:,-1])
        plt.scatter(self.zpos[:,-1],self.xpos[:,-1],s=2*np.power(F,.95),c=F,cmap=cmap,norm=norm)
        plt.xlim(-.75,.75)
        plt.ylim(-.75,.75)
        plt.grid(True)
        plt.colorbar()
        plt.xlabel('z position(m)')
        plt.ylabel('x position(m)')
                
    def Control_force_sesmic(self):    
        results_dir = os.path.join(self.results_dir, 'Total_Force/')     
        if not os.path.isdir(results_dir):
            os.makedirs(results_dir)
            cmap = plt.cm.get_cmap('seismic')
            F=abs(np.sqrt(np.power(self.Fcx[:,-1],2)+np.power(self.Fcz[:,-1],2)))
        
            NX=self.Fcx[:,-1]/F
            NZ=self.Fcz[:,-1]/F
            origin = self.zpos[:,-1],self.xpos[:,-1]
            boundaries=np.arange(np.amin(F),np.amax(F),.1)
            norm = colors.BoundaryNorm(boundaries, cmap.N, [boundaries[0], 100])
        plt.figure(3,figsize=(8,8),dpi=150)
        plt.quiver(*origin, NZ, NX, color="k", scale=21)
        plt.scatter(self.zpos[:,-1],self.xpos[:,-1],s=2*np.power(F,1),c=F,cmap=cmap,norm=norm)
        plt.xlim(-.75,.75)
        plt.ylim(-.75,.75)
        plt.grid(True)
        plt.colorbar()
        plt.xlabel('z position(m)')
        plt.ylabel('x position(m)')        
        
    def Control_force_sesmic_animation(self):    
        results_dir = os.path.join(self.results_dir, 'Total_Force/')     
        if not os.path.isdir(results_dir):
            os.makedirs(results_dir)
        count=0
        for i in range(1,len(self.time)-1):
            plt.figure(i,figsize=(8,8),dpi=150)    
            cmap = plt.cm.get_cmap('seismic')
            F=abs(np.sqrt(np.power(self.Fcx[:,i],2)+np.power(self.Fcz[:,i],2)))
            NX=self.Fcx[:,i]/F
            NZ=self.Fcz[:,i]/F
            origin = self.zpos[:,i],self.xpos[:,i]
            boundaries=np.arange(np.amin(F),np.amax(F),.1)
            norm = colors.BoundaryNorm(boundaries, cmap.N, [boundaries[0], 100])
            
            if i%3==0:
                count=count+1 
                plt.figure(i,figsize=(8,8),dpi=150)
                plt.quiver(*origin, NZ, NX, color="k", scale=21)
                plt.scatter(self.zpos[:,i],self.xpos[:,i],s=2*np.power(F,1.5),c=F,cmap=cm.hsv,norm=norm)
            
            
                plt.xlim(-.75,1.75)
                plt.ylim(-.75,.75)
                plt.grid(True)
                plt.colorbar()
                plt.title('t='+str(round(self.time[i],3)))
                plt.savefig(results_dir+"/picture"+str(count)+".jpg")  
                print(str(i)+ "of"+ str(len(self.time)))
                plt.close('all')
                plt.xlabel('z position(m)')
                plt.ylabel('x position(m)') 

    def plot_ball(self): 
        
        direct = os.path.join(self.results_dir)
        if not os.path.isdir(direct):
            os.makedirs(direct)
            
 
        fig=plt.figure(1)
        plt.figure(figsize=(15,10))
    
        fig.suptitle("Ball position vs time " )
        ax1 = plt.subplot(2,1,1)
        ax1.grid(True)
        plt.gca().set_title('X  vs time')
        plt.plot(self.time, self.ballx,'b')
    
        ax2 = plt.subplot(2,1,2)
        plt.gca().set_title('Z vs time')
        plt.plot(self.time,self.ballz,'r')
        ax2.grid(True)

        plt.subplots_adjust(hspace = 1)
        plt.xlabel('time (seconds)')
        plt.savefig(direct+"/ball_position.pdf") 
        #plt.close('all') 
# plotter for comparing robots        
class compare_cases:
    def __init__(self,cases,variable,nb):
        self.cases=cases
        self.variable=variable
        self.datax={}
        self.dataz={}
        self.cs=['r','g','b']
        self.nb=nb
        self.path='C:/Users/dmulr/OneDrive\Documents/dm-soro_chrono/python/Pychrono/Grabbing/Grab_sim_pot_field_shape/robot_data'
        self.filesx=self.path+str(self.cases[0])+"/X_points_shape.csv"
        self.filesy=self.path+str(self.cases[0])+"/Y_points_shape.csv"

        self.xd = np.genfromtxt(self.filesx,delimiter=',')
        self.yd = np.genfromtxt(self.filesy,delimiter=',')
        
        for i in self.cases:
    
            self.datax["d{0}".format(i)]=[]
            self.dataz["d{0}".format(i)]=[]
    
        for i in self.cases:

            patht=self.path+str(i)+'/'
            name='bot_position.csv'
            filename=patht+name
            data1 = np.genfromtxt(filename,delimiter=',')
            (m1,n1)=np.shape(data1)
            data1=data1[:,1:n1]

            xpos=data1[1:self.nb+1,:]
            ypos=data1[self.nb+1:2*self.nb+1,:]
            zpos=data1[(2*self.nb)+1:3*self.nb+1,:]
    
            zf=zpos[:,-1]
            xf=xpos[:,-1]
            self.datax["d"+str(i)].append(xf)
            self.dataz["d"+str(i)].append(zf)

    def plot_compare(self):
        
        count=0
        for i in self.cases:
            print(i)
            plt.scatter(self.dataz["d"+str(i)],self.datax["d"+str(i)],color=self.cs[count],label="alpha="+self.variable[count])
            count=count+1  
        plt.legend(loc='center left')
        plt.grid(True)
    
        
    def plot_compare_shape(self):
        
        count=0
        for i in self.cases:
            print(i)
            plt.scatter(self.dataz["d"+str(i)],self.datax["d"+str(i)],color=self.cs[count],label="alpha="+self.variable[count])
            count=count+1  
        plt.scatter(self.xd,self.yd,color="black",label="Desired")
        plt.legend(loc='center left')
        plt.grid(True)       
        
        
# Force chains
class plot_force_chain:
    def __init__(self,data0,data6,data7,data8,data9,data10,data11,data12,results_dir,nb):  
        self.data0=data0
        
        self.time=self.data0[0,:]
        self.xc=data6
        self.yc=data7
        self.zc=data8
        self.Fcx=data9
        self.Fcy=data10
        self.Fcz=data11
        self.nc=data12
        self.nb=nb
        self.results_dir=results_dir
    
    
    def Forcechains(self):
        direct = os.path.join(self.results_dir,'forcechain')    
        if not os.path.isdir(direct):
            os.makedirs(direct)
   
        cmap = plt.cm.get_cmap('seismic')
        boundaries=np.arange(10,30,.1)
                       
        norm = colors.BoundaryNorm(boundaries, cmap.N, [boundaries[0], 100])
        count=0
        for i in range(1,len(self.time)-1):
            Fx=self.Fcx[0:int(self.nc[i]),i]
            Fz=self.Fcz[0:int(self.nc[i]),i]
        #Fy=Fcy[0:nc[i],i]
            abs_force=np.power(np.add(np.power(Fx,2),np.power(Fz,2)),.5)


            x=self.xc[0:int(self.nc[i]),i]
            y=self.zc[0:int(self.nc[i]),i]
            x2=[]
            y2=[]
            F2=[]
            for j in range(len(abs_force)):
                if abs_force[j]>=1:
                    x2.append(x[j])
                    y2.append(y[j])
                    F2.append(abs_force[j])
               
  
            if i%3==0:
                count=count+1 
                plt.figure(i,figsize=(8,8),dpi=150)
                plt.scatter(y2,x2,s=2*np.power(F2,.65),c=F2,cmap=cmap,norm=norm)
                plt.xlim(-.75,1.75)
                plt.ylim(-.75,.75)
                plt.grid(True)
                plt.colorbar()
                plt.xlabel('x position(m)')
                plt.ylabel('z position(m)')
                plt.title('t='+str(round(self.time[i],3)))
                plt.savefig(direct+"/picture"+str(count)+".jpg")  
                print(str(i)+ "of"+ str(len(self.time)))
                plt.close('all')
            
            
class Packing_fraction:
    def __init__(self,data0,data15,nb,diameter,results_dir):
        self.nb=nb

        self.data0=data0
        (self.m0,self.n0)=np.shape(self.data0)
        self.data0=self.data0[:,1:self.n0]
        self.time=self.data0[0,:]
        self.xposb=self.data0[1:nb+1,:]
        self.yposb=self.data0[nb+1:2*nb+1,:]
        self.zposb=self.data0[(2*nb)+1:3*nb+1,:]
        self.results_dir=results_dir
        
        self.data15=data15
        (self.m15,self.n15)=np.shape(self.data15)
        self.data15=self.data15[:,1:self.n15]
        self.ni=int((self.m15-1)/3)
        self.time=self.data15[0,:]
        self.xposp=self.data15[1:self.ni+1,:]
        self.yposp=self.data15[self.ni+1:2*self.ni+1,:]
        self.zposp=self.data15[(2*self.ni)+1:3*self.ni+1,:]
       
        
        self.X=np.concatenate((self.xposb[:,-1],self.xposp[:,-1]), axis=None)
        self.Z=np.concatenate((self.zposb[:,-1],self.zposp[:,-1]), axis=None)
        self.P=np.stack((self.Z.T, self.X.T), axis=1)
        self.radius=None
        self.new_regions = []
        self.new_vertices=[]
        self.diameter=diameter
        self.A=[]
#        voronoi_plot_2d(self.vor)
#        plt.show()
        
##################################################################      
    def voronoi_finite_polygons_2d(self):
    

        if self.vor.points.shape[1] != 2:
            raise ValueError("Requires 2D input")

            # new vertices
        new_vertices = self.vor.vertices.tolist()


            # calculate center
        center = self.vor.points.mean(axis=0)
        if self.radius is None:
            self.radius = self.vor.points.ptp().max()*2

            # Construct a map containing all ridges for a given point
        all_ridges = {}
    
        for (p1, p2), (v1, v2) in zip(self.vor.ridge_points, self.vor.ridge_vertices):
            all_ridges.setdefault(p1, []).append((p2, v1, v2))
            all_ridges.setdefault(p2, []).append((p1, v1, v2))

            # Reconstruct infinite regions
        for p1, region in enumerate(self.vor.point_region):
            vertices = self.vor.regions[region]

            if all(v >= 0 for v in vertices):
            # finite region
                self.new_regions.append(vertices)
                continue

        # reconstruct a non-finite region
            ridges = all_ridges[p1]
            new_region = [v for v in vertices if v >= 0]

            for p2, v1, v2 in ridges:
                if v2 < 0:
                    v1, v2 = v2, v1
                if v1 >= 0:
                # finite ridge: already in the region
                    continue

            # Compute the missing endpoint of an infinite ridge
                t = self.vor.points[p2] - self.vor.points[p1] # tangent
                t /= np.linalg.norm(t)
                n = np.array([-t[1], t[0]])  # normal

                midpoint = self.vor.points[[p1, p2]].mean(axis=0)
                direction = np.sign(np.dot(midpoint - center, n)) * n
                far_point = self.vor.vertices[v2] + direction * self.radius

                new_region.append(len(new_vertices))
                new_vertices.append(far_point.tolist())

        # sort region counterclockwise
            vs = np.asarray([new_vertices[v] for v in new_region])
            c = vs.mean(axis=0)
            angles = np.arctan2(vs[:,1] - c[1], vs[:,0] - c[0])
            new_region = np.array(new_region)[np.argsort(angles)]

        # finish
            self.new_regions.append(new_region.tolist())

        return self.new_regions, np.asarray(new_vertices)    
###################################################################################    
    def voronoi_Area(self,region):
        Area=ConvexHull(self.vertices[region]).volume
        return(Area)
  #########################################################################  
    def calculate_packing(self):
        self.vor = Voronoi(self.P)
        (self.regions, self.vertices) = self.voronoi_finite_polygons_2d()
        
        Aact=np.pi*(self.diameter/2)**2
        for i in range(len(self.regions)):
            region=self.regions[i]
            self.A.append(Aact/(self.voronoi_Area(region)))
        self.A=np.asarray(self.A)

    def plot_packing(self):
        direct = os.path.join(self.results_dir)     
        if not os.path.isdir(direct):
            os.makedirs(direct)
        minima = min(self.A)
        maxima = max(self.A)

        #normalize chosen colormap
        norm = mpl.colors.Normalize(vmin=minima, vmax=maxima, clip=True)
        mapper = cm.ScalarMappable(norm=norm, cmap=cm.hsv)


        plt.figure(figsize=(13,13))

        for r in range(len(self.vor.point_region)):
            region = self.vor.regions[self.vor.point_region[r]]
            if not -1 in region:
                polygon = [self.vor.vertices[i] for i in region]
                plt.fill(*zip(*polygon), color=mapper.to_rgba(self.A[r]))
        #plt.axis('equal')
        #plt.xlim(self.vor.min_bound[0] - 0.1, self.vor.max_bound[0] + 0.1)
        #plt.ylim(self.vor.min_bound[1] - 0.1, self.vor.max_bound[1] + 0.1)
        Abar = self.A[self.A > .3]
        plt.xlim(0,1.2)
        plt.ylim(-.5,.5)
        plt.xlabel('x position (meters)')
        plt.ylabel('y position (meters)')
        plt.title('Local Packing fraction'+"avg phi="+str(round(statistics.mean(Abar),3)))
        plt.colorbar(mapper)
        plt.savefig(direct+"/Packing_fraction.pdf")
        plt.close('all')

    def stat_packing(self):
        direct = os.path.join(self.results_dir)     
        if not os.path.isdir(direct):
            os.makedirs(direct)
        n, bins, patches = plt.hist(self.A, 20, facecolor='g')


        plt.xlabel('local Packing fraction')
        plt.ylabel('number of occurances')
        Abar = self.A[self.A > .5]
        print(statistics.mean(Abar)) 
        plt.title('Histogram of packing fraction')
        plt.savefig(direct+"/statistics.pdf")
        plt.close('all')
        #plt.text(60, .025, r'$\mu=100,\ \sigma=15$')
        #plt.xlim(.7,.9)
        #plt.ylim(0, 0.03)

#plt.grid(True)
#plt.savefig("distribution 8 robot 15 particles"+name+".png")
#plt.show()     

