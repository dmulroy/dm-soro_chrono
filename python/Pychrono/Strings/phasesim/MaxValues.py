# In[Header]
"""
author: declan mulroy
project: JAMoEBA
email: dmulroy@hawk.iit.edu
date: 12/4/19
Max values
"""

import os
import numpy as np


nb=100             # number of robots
diameter=.07        # diameter of cylinder and robots
diameter2=.07*(2**(.5))


R1=(diameter*nb/(np.pi*2))
Rin=R1

ngrans1=int(Rin/(diameter+diameter2))


ri=np.zeros((1,2*ngrans1))
ni=np.zeros((1,2*ngrans1))

    
radii=Rin-(diameter/2)
for i in range(ngrans1):
    print(i)
    remainder1=((diameter2/2))
    remainder2=((diameter/2))
    ri[:,2*i]=radii-remainder1
    ri[:,2*i+1]=ri[:,2*i]-remainder2
    radii=ri[:,2*i+1]
    
    ni[:,2*i]=np.floor((ri[:,2*i]*np.pi*2)/diameter2)

    ni[:,2*i+1]=np.floor((ri[:,2*i+1]*np.pi*2)/diameter)

    
ni=np.asarray(ni,dtype=int)

print(ni)