# -*- coding: utf-8 -*-
"""
Created on Tue Jan 28 10:01:39 2020

@author: dmulr
"""
from pyparticleio.ParticleCloud import ParticleCloud
import json
from numpy import zeros, sin, cos, ones, vdot, identity, pi, argmin, array
from math import atan2, sqrt
from scipy.optimize import LinearConstraint, Bounds, minimize, linprog
import rospy
from std_msgs.msg import String
from time import sleep, clock


def get_opt_result(self, theta):
    """ Method to get the result of the optimization """
    result = zeros(10)
    theta %= 360

# Simple comparison
    if self.opt == "comparison":
        for (i, ids) in enumerate(self.leg_ids):
            if -self.t_a <= theta[ids] <= self.t_a:
                result[i] = 1  # Go forward
            elif theta[ids] <= -(180 - self.t_a) or theta[ids] >= 180 - self.t_a:
                result[i] = -1  # Go backward
            else:
                result[i] = 0  # Vibrate

        # Minimize optimization
    elif self.opt == "minimize":
        res = minimize(self._f, self.x0, method='trust-constr', jac=self._f_der, hess=self._f_hess,
                          options={'verbose': 1}, bounds=self.bounds, constraints=[self.linear_constraint])
        for i in range(len(res)):
                result[i] = int(round(res.x[i]))

        # Simplex optimization
    elif self.opt == "simplex":
        res = linprog(sin(self.t) ** 2 - cos(self.t) ** 2, method='simplex', bounds=[(0, 1)] * 10)
        for (i, ids) in enumerate(self.leg_ids):
            if self.at_least_one_stopped:
                if (90 + self.t_a) < theta[ids] < (270 - self.t_a):
                    result[i] = -1
                elif 0 <= theta[ids] < (90 - self.t_a) or (270 + self.t_a) < theta[ids] <= 360:
                    result[i] = 1
                else:
                    result[i] = 0
            else:
                result[i] = -res.x[i] * (abs(self.t[i]) > np.pi / 2) + res.x[i] * (abs(self.t[i]) < np.pi / 2)

        return result

def state_callback(self, data):
    """ Method to send the desired signals to the photons """
    data_dict = eval(data.data)

    alpha = self.get_alpha(data_dict)
    angle = self.get_angle(data_dict)
    dist = self.get_dist(data_dict)
    closest, leg_closest, vac_closest = self.get_min(dist=dist)
    theta = zeros(self.num)

    self.stopped_robot = sum(1 * (self.stop_dist == self.stop_dist_default + 5))
    self.at_least_one_stopped = self.stopped_robot >= 1  # At least one stopped

    if self.at_least_one_stopped:  # If one robot is stopped
        self.angle_mode = "abs_closest"
    else:
        self.angle_mode = "relative"

    # Get the angles values of the legged robot and put them in a array for the optimization
    for (i, ids) in enumerate(self.leg_ids):
        if self.angle_mode == "relative":
            theta[ids] = angle[ids] - data_dict[str(ids)][2]
        elif self.angle_mode == "closest":
            theta[ids] = angle[closest] - data_dict[str(ids)][2]
        elif self.angle_mode == "mean":
            theta[ids] = alpha - data_dict[str(ids)][2]
        elif self.angle_mode == "abs_closest":
            theta[ids] = data_dict[str(self.get_stopped_closest(robot_id=ids, data_dict=data_dict))][2] - data_dict[str(ids)][2]
        self.t[i] = pi * theta[ids] / 180.

# Get the result of the desired optimization
    result = self.get_opt_result(theta=theta)

        # Set the right values for the legged robots
    for (i, ids) in enumerate(self.leg_ids):
        if result[i] == 1:
            self.state[str(ids)] = 1  # Move forward
        elif result[i] == -1:
            self.state[str(ids)] = 2  # Move backward
        elif result[i] == 0:
            self.state[str(ids)] = 3  # Vibrate
        else:
            self.state[str(ids)] = 0  # Stop

        # Toggle self.vac_state to put the vacuum on and off
    if (clock() - self.last) > 0.5:
        self.vac_state = not self.vac_state
        self.last = clock()

        # Set the right values for the vacuum robots
    for ids in self.vac_ids:
        theta[ids] = alpha - data_dict[str(ids)][2]
        if self.vacuum and self.vac_state:
            self.state[str(ids)] = 1  # Vacuum
        else:
            self.state[str(ids)] = 2  # Vibration

        # Stop if you get close to the target
    for i in range(self.num):
        if dist[i] < self.stop_dist[i]:
                # if sum(1*(i == self.vac_ids)) == 1:  # If it's a vacuum robot
                #     self.state[str(i)] = 1  # Vacuum
                # else:
            self.state[str(i)] = 0  # Stop
            self.stop_dist[i] = self.stop_dist_default + 5
        else:
            self.stop_dist[i] = self.stop_dist_default

        # Override function
    for i in range(self.num):
        if self.is_override[i]:
            self.state[str(i)] = int(self.override_state[i])

        # Send the values
    self.all_devices[self.first_device_name].publish("state", str(self.state))
