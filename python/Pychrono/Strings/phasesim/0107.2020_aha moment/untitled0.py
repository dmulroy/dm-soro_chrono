# -*- coding: utf-8 -*-
"""
Created on Sun Jan 19 18:24:48 2020

@author: dmulr
"""
import numpy as np
import math as math
import matplotlib.pyplot as plt
import os


# In[Extract one time step]


def extract_instance(qx,qz,i,ballp,nb):
    
    X=qx[nb*i:nb*i+nb]
    Z=qx[nb*i:nb*i+nb]
    B=ballp[i]
    
    return(X,Z,B)

# In[Determine Gripping angle]


 
#def Gripping_angle(ballp,nb,time,obj,i,xw):    
    
def Gripping_angle(X,Z,B):  
    # First deterime which quardant each robot falls in    
    # z plane    
    orignz=0
    # robots in quadrant 1
    bin1=[]
    # robots in quadrant 2
    bin2=[]
    
    # sort them into different bins
    for i in range(nb):
        q=orignz  # dummy variable 
        # if its positive
        if q>0:
            bin1.append(i)
        # if its negative
        elif q<0:
            bin2.append(i)
        # if it equals zero
        elif q==0:
            bin2.append(i)

    
    return(bin1,bin2)    
    
    
    
    # # z plane    
    # orignz=0
    # # robots in quadrant 1
    # bin1=[]
    # # robots in quadrant 2
    # bin2=[]
    
    # # sort them into different bins
    # for i in range(nb):
    #     q=orignz+obj[i].GetPos().z  # dummy variable 
    #     # if its positive
    #     if q>0:
    #         bin1.append(i)
    #     # if its negative
    #     elif q<0:
    #         bin2.append(i)
    #     # if it equals zero
    #     elif q==0:
    #         bin2.append(i)

    # xbin1=[]
    # zbin1=[]
    # xbin2=[]
    # zbin2=[]
    # # get positions for quadrant 1
    # for i in bin1:
    #     q=orignz+obj[i].GetPos().x
    #     w=orignz+obj[i].GetPos().z
        
    #     xbin1.append(q)
    #     zbin1.append(w)
    # maxx1=np.amax(xbin1)   
    # # get positions for quadrant 2
    # for i in bin2:
    #     q=orignz+obj[i].GetPos().x
    #     w=orignz+obj[i].GetPos().z
        
    #     xbin2.append(q)
    #     zbin2.append(w)
        
    # maxx2=np.amax(xbin2)                 

    # # the robot with the value         
    # ind1=np.where(xbin1 == maxx1)     
    # ind2=np.where(xbin2 == maxx2)
    
    
        
    
    # Xprime=np.zeros(nb,1)
    # Zprime=np.zeros(nb,1)
    
    # ii=time[i]
    # xtemp=np.asarray(Xpos)
    # ztemp=np.asarray(Zpos)
    
    # Xprime[:,0]=xtemp[nb*ii:nb*ii+nb]
    # Zprime[:,0]=ztemp[nb*ii:nb*ii+nb]
    
    # Xmax=np.amax(Xprime)
    
    # ind=np.where(Xprime == Xmax)
    
    # l=len(ind[0])
    
    # points=[]
    # for i in range(l):

    #     entry=ind[0][i]
    #     points.append(Zprime[entry])   
        







# In[Import Data]

#In[Import data]
data=np.load('Experiment 1.npz',allow_pickle=True)

#data2=np.load('compare.npz',allow_pickle=True)

# Positions
qx=data['qx']
qy=data['qy']
qz=data['qz']

# rotations
rot0=data['rot0']
rot1=data['rot1']
rot2=data['rot2']
rot3=data['rot3']

# Spring values
SL=data['SL']

Fmem=data['Fmem']
# Velocity
Xv=data['Xv']
Yv=data['Yv']
Zv=data['Zv']

# Total foces
Fxt=data['Fxt']
Fyt=data['Fyt']
Fzt=data['Fzt']

# Active robots
botcall=data['botcall']

# number of robots
nb=data['nb']
ni=data['ni']
# total number
nt=nb+ni
# diameter
diameter=data['diameter']
# height
height=data['height']
# time
time=data['ttemp']

sim=data['sim']
# for time
count=data['count']
# save file as 
file=".pdf"
# script directory
script_dir = os.path.dirname("plots"+str(sim)+"/")
# ball position
ballp=data['ballp']



# Contact points
xc=data['xc']
yc=data['yc']
zc=data['zc']
nc=data['nc']

# contact forces
Fcx=data['Fcx']
Fcy=data['Fcy']
Fcz=data['Fcz']

# normal direction
VNX=data['VNX']

VNZ=data['VNZ']

VTX=data['VTX']

VTZ=data['VTZ']


(X,Z,B)=extract_instance(qx,qz,i,ballp,nb)

