# -*- coding: utf-8 -*-
"""
Created on Thu Nov 14 17:56:34 2019

@author: dmulr
"""

import pychrono.core as chrono
import pychrono.irrlicht as chronoirr
import pychrono.postprocess as postprocess
import os
import numpy as np
import timeit

# In[Create material]
def Material(mu_f,mu_b,mu_r,mu_s,C,Ct,Cr,Cs):
    material = chrono.ChMaterialSurfaceNSC()
    material.SetFriction(mu_f)
    material.SetDampingF(mu_b)
    material.SetCompliance (C)
    material.SetComplianceT(Ct)
    material.SetRollingFriction(mu_r)
    material.SetSpinningFriction(mu_s)
    material.SetComplianceRolling(Cr)
    material.SetComplianceSpinning(Cs)
    return material

# In[Create Floor]
def Floor(material,length,tall):
    body_floor = chrono.ChBody()
    body_floor.SetBodyFixed(True)
    body_floor.SetPos(chrono.ChVectorD(0, -tall, 0 ))
    body_floor.SetMaterialSurface(material)
    body_floor.GetCollisionModel().ClearModel()
    body_floor.GetCollisionModel().AddBox(length, tall, length) # hemi sizes
    body_floor.GetCollisionModel().BuildModel()
    body_floor.SetCollide(True)
    body_floor_shape = chrono.ChBoxShape()
    body_floor_shape.GetBoxGeometry().Size = chrono.ChVectorD(length, tall, length)
    body_floor.GetAssets().push_back(body_floor_shape)
    black = chrono.ChColorAsset()
    black.SetColor(chrono.ChColor(0,0,0))
    body_floor.AddAsset(black)
    body_floor.SetId(0)
    return(body_floor)

# In[Interior Granulars]
def Interior(x,y,z,i,diameter,height,rowp,R2,material,obj,my_system,body_floor):
    gran = chrono.ChBody()
    gran = chrono.ChBodyEasyCylinder(diameter/2, height, rowp)
    gran.SetPos(chrono.ChVectorD(x,y,z))
    gran.SetMaterialSurface(material)
    gran.SetId(i)
    gran.GetCollisionModel().ClearModel()
    gran.GetCollisionModel().AddCylinder(diameter/2,diameter/2,height/2) # hemi sizes
    gran.GetCollisionModel().BuildModel()
    gran.SetCollide(True)
    gran.SetBodyFixed(False)
    col_r = chrono.ChColorAsset()
    col_r.SetColor(chrono.ChColor(1, 0, 0))
    gran.AddAsset(col_r)
    pt=chrono.ChLinkMatePlane()
    pt.Initialize(body_floor,gran,False,chrono.ChVectorD(0,0,0),chrono.ChVectorD(0,0,0),chrono.ChVectorD(0,1, 0),chrono.ChVectorD(0,-1, 0))
    my_system.AddLink(pt)
    
    my_system.Add(gran)
    obj.append(gran)
    return my_system,obj 
    
# In[Ball]
def Ball(x,y,z,Rb,height,hhalf,rowr,material,obj,my_system,forceb,body_floor,Balls):
    
    z2x = chrono.ChQuaternionD()
    z2x.Q_from_AngAxis(chrono.CH_C_PI / 2, chrono.ChVectorD(0, 1, 0))
    ball = chrono.ChBody()
    ball = chrono.ChBodyEasyCylinder(Rb,height,rowr)
    ball.SetPos(chrono.ChVectorD(x,y,z))
    ball.SetMaterialSurface(material)
    myforcex = chrono.ChForce()
    ball.AddForce(myforcex)
    myforcex.SetMode(chrono.ChForce.FORCE)
    myforcex.SetDir(chrono.VECT_X)
    myforcex.SetVrelpoint(chrono.ChVectorD(x,.03*y,z))
    #myforcex.SetMforce(mag)
    forceb.append(myforcex)
    # collision model
    ball.GetCollisionModel().ClearModel()
    ball.GetCollisionModel().AddCylinder(Rb,Rb,hhalf) # hemi sizes
    ball.GetCollisionModel().BuildModel()
    ball.SetCollide(True)
    ball.SetBodyFixed(False)
    col_b = chrono.ChColorAsset()
    col_b.SetColor(chrono.ChColor(0, 0, 1))
    ball.AddAsset(col_b)
    pt=chrono.ChLinkMatePlane()
    pt.Initialize(body_floor,ball,False,chrono.ChVectorD(0,0,0),chrono.ChVectorD(0,0,0),chrono.ChVectorD(0,1, 0),chrono.ChVectorD(0,-1, 0))
    prismatic_ground_ball = chrono.ChLinkLockPrismatic()
    prismatic_ground_ball.SetName("prismatic_ground_ball")
    prismatic_ground_ball.Initialize(body_floor, ball, chrono.ChCoordsysD(chrono.ChVectorD(5.5, 0, 0), z2x))
    my_system.AddLink(prismatic_ground_ball)
    
    my_system.AddLink(pt)
    my_system.Add(ball)
    obj.append(ball)
    Balls.append(ball)
# In[Def Box]
def Box(x,y,z,Rb,RL,height,hhalf,rowr,material,obj,my_system,forceb,body_floor,ball):
    
    z2x = chrono.ChQuaternionD()
    z2x.Q_from_AngAxis(chrono.CH_C_PI/2, chrono.ChVectorD(0, 1, 0))
    ball = chrono.ChBody()
    ball = chrono.ChBodyEasyBox(2*Rb,height,2*RL,rowr)
    ball.SetPos(chrono.ChVectorD(x,y,z))
    ball.SetRot(chrono.Q_from_AngY(np.pi/4))
    ball.SetMaterialSurface(material)
    myforcex = chrono.ChForce()
    ball.AddForce(myforcex)
    myforcex.SetMode(chrono.ChForce.FORCE)
    myforcex.SetDir(chrono.VECT_X)
    myforcex.SetVrelpoint(chrono.ChVectorD(x,.03*y,z))
    #myforcex.SetMforce(mag)
    forceb.append(myforcex)
    # Collision shape
    ball.GetCollisionModel().ClearModel()
    ball.GetCollisionModel().AddBox(Rb,hhalf,Rb) # must set half sizes
    ball.GetCollisionModel().BuildModel()
    ball.SetCollide(True)
    ball.SetBodyFixed(False)
    col_b = chrono.ChColorAsset()
    col_b.SetColor(chrono.ChColor(0, 0, 1))
    ball.AddAsset(col_b)
    pt=chrono.ChLinkMatePlane()
    pt.Initialize(body_floor,ball,False,chrono.ChVectorD(0,0,0),chrono.ChVectorD(0,0,0),chrono.ChVectorD(0,1, 0),chrono.ChVectorD(0,-1, 0))
    prismatic_ground_ball = chrono.ChLinkLockPrismatic()
    prismatic_ground_ball.SetName("prismatic_ground_ball")
    prismatic_ground_ball.Initialize(body_floor, ball, chrono.ChCoordsysD(chrono.ChVectorD(5.5, 0, 0), z2x))
    my_system.AddLink(prismatic_ground_ball)
    
    my_system.AddLink(pt)
    my_system.Add(ball)
    obj.append(ball)    
    return (forceb,obj,ball)
# In[Wall]
def Wall(x,y,z,rotate,length,height,width,material,my_system):
    body_brick = chrono.ChBody()
    body_brick.SetBodyFixed(True)
    # set initial position
    body_brick.SetPos(chrono.ChVectorD(x,y,z))
    body_brick.SetRot(chrono.Q_from_AngY(rotate))
    # set mass properties
    body_brick.SetMass(.5)
    body_brick.SetInertiaXX(chrono.ChVectorD(1,1,1))
    # set collision surface properties
    body_brick.SetMaterialSurface(material)
    # Collision shape
    body_brick.GetCollisionModel().ClearModel()
    body_brick.GetCollisionModel().AddBox(length/2, height/2, width/2) # must set half sizes
    body_brick.GetCollisionModel().BuildModel()
    body_brick.SetCollide(True)
    # Visualization shape, for rendering animation
    body_brick_shape = chrono.ChBoxShape()
    body_brick_shape.GetBoxGeometry().Size = chrono.ChVectorD(length/2, height/2, width/2)
    col_brick = chrono.ChColorAsset()
    col_brick.SetColor(chrono.ChColor(1,0.1,0.5))     #Pink
    body_brick.AddAsset(col_brick)
    body_brick.GetAssets().push_back(body_brick_shape)
    body_brick.GetAssets().push_back(col_brick)
    my_system.Add(body_brick)    
 
    # Radius inside
# In[Call springs to be jammed]
def Jamsprings(k,rl,rlmax,Springs,t,tj,Fm,kj,rlj,rljmax,i,jamcall):
    if t>tj:
        if jamcall==1:
            k=kj
            rl=rlj
            rlmax=rljmax
            
        if jamcall==2 and t>tj+0.05:
            k=-kj/10
            rl=rlj
            rlmax=rljmax
            
        else:
            k=k
            rl=rl
            rlmax=rlmax
    else:
        k=k
        rl=rl
        rlmax=rlmax
        
    return(k,rlmax,rl)

# In[Number of contacts]
    
class MyReportContactCallback(chrono.ReportContactCallback):

    def __init__(self):
        chrono.ReportContactCallback.__init__(self)
        self.Fcx=[]
        self.Fcy=[]
        self.Fcz=[]
        self.pointx = []
        self.pointy = []
        self.pointz = []
        self.bodies = []
        
    def OnReportContact(self,vA,vB,cA,dist,rad,force,torque,modA,modB):
        bodyUpA = chrono.CastContactableToChBody(modA)
        nameA = bodyUpA.GetId()
        bodyUpB = chrono.CastContactableToChBody(modB)
        nameB = bodyUpB.GetId()
        self.pointx.append(vA.x)
        self.pointy.append(vA.y)
        self.pointz.append(vA.z)
        self.Fcx.append(force.x)
        self.Fcy.append(force.y)
        self.Fcz.append(force.z)
        self.bodies.append([nameA,nameB])
        return True  # return False to stop reporting contacts

    # reset after every run 
    def ResetList(self):
        self.pointx = []
        self.pointy = []
        self.pointz = [] 
        self.Fcx=[]
        self.Fcy=[]
        self.Fcz=[]
        self.bodies = []
        
    # Get the points
    def GetList(self):
        return (self.pointx,self.pointy,self.pointz,self.Fcx,self.Fcy,self.Fcz, self.bodies)
    

