# -*- coding: utf-8 -*-
"""
Created on Thu Mar  5 22:27:48 2020

email: elopez8@hawk.iit.edu
@author: Esteban Lopez
date: 3/5/2020

https://stable-baselines.readthedocs.io/en/master/guide/rl_tips.html
"""
import pychrono as chrono
try:
    from pychrono import irrlicht as chronoirr
except:
    print('Could not import ChronoIrrlicht')
import pychrono.postprocess as postprocess
import numpy as np
from numpy import mean
from gym import spaces, Env
from math import floor
import os
from gym_chrono.envs.ChronoBase import  ChronoBaseEnv
from Sim_objects import Material, Floor, Interior, MyReportContactCallback
from datetime import datetime
import matplotlib.pyplot as plt


class Strings(Env):
    
# In[Init Function]    
    def __init__(self, experiment_name = 'NOT NAMED', data_collect = False, plot = False, POV_Ray=False):
        #-----------------------------
        #     Create the System
        #     Set System parameters
        #-----------------------------
        self.system = chrono.ChSystemNSC()
        
        self.time = 0 # Using this to keep track of what is going on and when
        self.timestep= 0.01
        chrono.ChCollisionModel.SetDefaultSuggestedEnvelope(0.001)
        chrono.ChCollisionModel.SetDefaultSuggestedMargin(0.001)
        self.system.Set_G_acc(chrono.ChVectorD(0,-9.81,0))
        self.system.SetSolverMaxIterations(70)
        
        self.info = {"timeout": 10000.0}
        
        #-----------------------------------
        #     Bot and Spring Parametesrs
        #-----------------------------------
        self.num_bots= 10                                  # number of robots
        self.diameter = 0.07                               # Diameter of robots and interior
        self.mass = 0.18                                   # Mass of robots
        self.height = 0.12                                 # Height of cylinder
        self.R1=(self.diameter*self.num_bots/(np.pi*2))+.1 # Radius of rings of bots + 10cm
        self.k = 50                                       # Spring constant (bots)
        self.k_stiff = self.k*5                           # If the spring is extended too far, then the stiffness will increase by a factor of 10
        self.b = self.k/50                                 # Damping (bots)
        self.rl = 0.002                                    # Resting Length
        self.spring_max = 0.05                             # If springs get this long, they will stiffen to self.k_stiff to avoid interior spilling out
        self.volume = np.pi*.25*self.height*(self.diameter)**2
        self.rho_bot=self.mass/self.volume
        
        self.gain=2.0                                      # Defines the gain on the force from action recommendations!
        
        #---------------------------------------
        #     Target position for COG of JAMoEBA
        #---------------------------------------
        self.X_targ = 1
        self.Z_targ = 0
        self.d_old = np.linalg.norm(self.X_targ + self.Z_targ)
        
        #--------------------------
        #       Wall Parameters
        #--------------------------
        self.wall_width=4
        self.wall_height = self.height
        self.wall_thickness = self.R1*2
        self.wall_gap = self.R1*1.5
        self.wall_z = 0.5*self.wall_gap + 0.5*self.wall_width
        self.wall_x = self.R1*2
        
        #----------------------------------------------
        #       Setting up number of Interior granulars
        #----------------------------------------------
        self.in_rings_radius = []                      # Radius of interior rings
        self.gran_per_ring = []
        current_circumference = 2*np.pi*self.R1
        current_radius = self.R1
        while current_circumference > (self.diameter*3): # Not allowing an interior ring with less than 3 granulars
            current_radius = current_radius - (self.diameter + 0.01) # A 1 centimeter buffer is thrown in to keep add space
            current_circumference = 2*np.pi*current_radius
            self.in_rings_radius.append(current_radius)
            
        if current_circumference > np.pi*self.diameter:
            self.in_rings_radius.append(0)             # If the space allows it, let's throw one more interior iin the center
            
        for radius in self.in_rings_radius:
            current_num_interior = floor((2*np.pi*radius)/self.diameter) # Calcualates how many interior granules fit in each interior ring
            self.gran_per_ring.append(current_num_interior)
            
            if radius == 0:
                self.gran_per_ring.append(1) #Throw in a center granule
                
        self.num_interior = sum(self.gran_per_ring)
        #-------------------------
        #     Floor Parameters
        #-------------------------
        self.length=500 # Floor length and width
        self.tall=0.1   # Floor height

        #----------------------------
        #     Bot and Particle Material Properties
        #----------------------------
        self.mu_f = 0.4     # Friction
        self.mu_b = 0.01    # Damping
        self.mu_r = 0.4     # Rolling Friction
        self.mu_s = 0.1     # Spinning Friction

        self.Ct = 0.00001
        self.C = 0.00001
        self.Cr = 0.0001
        
        
        self.Cs = 0.0001
        self.material = Material(self.mu_f, self.mu_b, self.mu_r, self.mu_s, self.C, self.Ct, self.Cr, self.Cs)

        #-------------------------------
        #     Floor and Obstacle(s) Material Properties
        #-------------------------------
        self.mu_f2 = 0.1     # Friction
        self.mu_b2 = 0.01    # Damping
        self.mu_r2 = 0.4     # Rolling Friction
        self.mu_s2 = 0.2     # Spinning Friction
        self.material2 = Material(self.mu_f2, self.mu_b2, self.mu_r2, self.mu_s2, self.C, self.Ct, self.Cr, self.Cs)
        
        #----------------------------
        #       Set up the gym API
        #----------------------------
        #ChronoBaseEnv.__init__(self)
        self.render_setup = False

        self.state_size = self.num_bots*8 + self.num_interior*4
        # State is: [Bot_Xpos, Bot_Zpos, Bot_Xvel, Bot_Zvel, Bot_action_x, 
        # Bot_action_z, Bot_F_external_x, Bot_F_external_z, in_Xpos, in_Zpos, in_Xvel, in_Zvel] for each bot
        self.action_size = self.num_bots*2
        # Action is: [Bot_force_x, Bot_force_z] for each bot
        
        low = np.full(self.state_size, -1000)
        high = np.full(self.state_size, 1000)
        self.observation_space = spaces.Box(low, high, dtype=np.float32)

        #Change the number in 'shape(x,)' where x is the number of actions needed. In this case, 6 (2 forces per self.bot)
        self.action_space = spaces.Box(low=-1.0, high=1.0, shape=(self.action_size,), dtype=np.float32) #Recommended to make the action space units!
        
        #-----------------------------------------
        #       Setup Matrices and Data Collection
        #-----------------------------------------
        self.data_collect = data_collect
        self.plot = plot
        self.experiment_name = experiment_name
        self.POV_Ray = POV_Ray
        
        self.environment_parameters = [['X_Targ:',str(self.X_targ)], 
                                       ['Z_targ:', str(self.Z_targ)], 
                                       ['Num_Bots:', str(self.num_bots)],
                                       ['Num_Interior:', str(self.num_interior)],
                                       ['Bot_Diameter:', str(self.diameter)], 
                                       ['Bot_Height:',str(self.height)], 
                                       ['Spring_k:', str(self.k)], 
                                       ['Spring_b:', str(self.b)], 
                                       ['Spring_rl:', str(self.rl)], 
                                       ['Force_Gain:', str(self.gain)]]
        
        if self.data_collect:
            now=str(datetime.now())
            now=now.replace(":","")
            now=now[:-7]
    
            self.new_folder = experiment_name + ' Data and Plots ' + now + "/"
            
            if not os.path.exists(self.new_folder):
                os.makedirs(self.new_folder)
            
            self.X_data = np.zeros(self.num_bots + 1)
            self.X_vel_data = np.zeros(self.num_bots + 1)
            self.Y_data = np.zeros(self.num_bots + 1)
            self.Y_vel_data = np.zeros(self.num_bots + 1)
            self.Z_data = np.zeros(self.num_bots + 1)
            self.Z_vel_data = np.zeros(self.num_bots + 1)
            self.force_data = np.zeros(self.num_bots*2 + 1)
            self.ac = np.zeros(self.action_size + 1)
            self.reward_data = np.zeros(2)
            
# In[Reset Function]    
    """
    Reset Function - Gets called first, sets up the system
    """
    def reset(self):
        self.isdone = False
        self.system.Clear()

        #---------------------------------------
        #     Empty vectors for storing objects
        #---------------------------------------
        self.bots = []         # Store bots!
        self.interior = []     # Store interior granules
        self.forces = []       # Store the force objects!
        self.Springs = []      # Store the spring. Nothing done with this yet.
        self.obstacle_ids = [] # Store the IDs of obstacles
        
        self.X_Pos = []   # Store X-pos for reward processing
        self.Z_Pos = []   # Store Z-Pos for reward processing
        
        self.actions_taken = 0 #This will be used to calculate how many actions are being taken versus how many actually need to be taken
        self.total_force_needed = 1140*2 # Emperically found
        
        self.my_rep = MyReportContactCallback() # Will be used for collision detection

        #------------------------
        #     Create the Floor
        #------------------------
        self.body_floor = Floor(self.material2, self.length, self.tall)
        self.body_floor.SetId(0)
        self.system.Add(self.body_floor)

        #---------------------------
        #     Make the bots!
        #---------------------------
        for i in range(self.num_bots): 
            theta=i*2*np.pi/self.num_bots
            x=self.R1*np.cos(theta)
            y=0.5*self.height
            z=self.R1*np.sin(theta)
            # Create bots    
            self.bot = chrono.ChBody()
            self.bot = chrono.ChBodyEasyCylinder(self.diameter/2, self.height, self.rho_bot)
            self.bot.SetPos(chrono.ChVectorD(x,y,z))
            self.bot.SetMaterialSurface(self.material)
            # rotate them
            rotation1 = chrono.ChQuaternionD()
            rotation1.Q_from_AngAxis(-theta, chrono.ChVectorD(0, 1, 0));  
            self.bot.SetRot(rotation1)

        #----------------------
        #     Collision model
        #----------------------
            self.bot.GetCollisionModel().ClearModel()
            self.bot.GetCollisionModel().AddCylinder(self.diameter/2, self.diameter/2, self.height/2) # hemi sizes
            self.bot.GetCollisionModel().BuildModel()
            self.bot.SetCollide(True)
            self.bot.SetBodyFixed(False)
            pt=chrono.ChLinkMatePlane()
            pt.Initialize(self.body_floor,self.bot,False,chrono.ChVectorD(0,0,0),chrono.ChVectorD(0,0,0),chrono.ChVectorD(0,1, 0),chrono.ChVectorD(0,-1, 0))
            self.system.AddLink(pt)

        #---------------------------------
        #     Apply forces to bots
        #---------------------------------

            # X-Direction Force
            self.forcex = chrono.ChForce()
            self.bot.AddForce(self.forcex)
            self.forcex.SetMode(chrono.ChForce.FORCE)
            self.forcex.SetDir(chrono.VECT_X)
            self.forcex.SetVrelpoint(chrono.ChVectorD(0,0,0)) # Force acts on the center of the bots
            #myforcex.SetMforce(mag)
            self.forces.append(self.forcex) # Add the X-Force into the forces array

            # Z-Direction Force
            self.forcez=chrono.ChForce()
            self.bot.AddForce(self.forcez)
            self.forcez.SetMode(chrono.ChForce.FORCE)
            self.forcez.SetDir(chrono.VECT_Z)
            self.forcez.SetVrelpoint(chrono.ChVectorD(0,0,0)) # Force acts on the center of the bots
            self.forces.append(self.forcez) # Add the Z-Force into the forces array
            
            col_y = chrono.ChColorAsset()
            col_y.SetColor(chrono.ChColor(0.44, .11, 52))
            self.bot.AddAsset(col_y)
            
            self.bot.SetId(i+1) # Bots have an ID starting from 1
            self.bots.append(self.bot) # But will this retain the springs added below?
            
        #---------------------------
        #     Attach Springs
        #---------------------------    
            col1 = chrono.ChColorAsset()
            col1.SetColor(chrono.ChColor(0,0,1))
            
            if i>=1:
                self.spring=chrono.ChLinkSpring()
                
                # Identify points to be attatched to the springs 
                self.spring.SetName("self.spring")
                p1=0
                p2=self.diameter/2
                p3=0
                p4=-self.diameter/2
                #h=self.height/4
                h=0 # Based off phase_sim_sqr_code-parallel.cpp
                
                # Attatches first springs
                self.spring.Initialize(self.bots[i-1], self.bot,True,chrono.ChVectorD(p1,h,p2), chrono.ChVectorD(p3,h,p4),False)
                self.spring.Set_SpringK(self.k)
                self.spring.Set_SpringR(self.b)
                self.spring.Set_SpringRestLength(self.rl)
                self.spring.AddAsset(col1)
                self.spring.AddAsset(chrono.ChPointPointSpring(.01,80,15))
                self.system.AddLink(self.spring)
                self.Springs.append(self.spring)
                
        # Last spring
        #if i==self.num_bots-1:        
        self.spring=chrono.ChLinkSpring()
        self.spring.SetName("self.spring")
        self.spring.Initialize(self.bots[self.num_bots - 1], self.bots[0], True, chrono.ChVectorD(p1,h,p2), chrono.ChVectorD(p3,h,p4),False)
        self.spring.Set_SpringK(self.k)
        self.spring.Set_SpringR(self.b)
        self.spring.Set_SpringRestLength(self.rl)
        self.spring.AddAsset(col1)
        self.spring.AddAsset(chrono.ChPointPointSpring(.01,80,15))
        self.system.AddLink(self.spring)
        self.Springs.append(self.spring) 
            
        for i in self.bots:
            self.system.Add(i)
        
        #---------------------
        #       Add Interior
        #---------------------
        i=0
        n = self.num_bots + 1 # This is the first index ID for particles
        for radius in self.in_rings_radius:
            if radius != 0: # Placing interiors wherever there is a ring
                for j in range(self.gran_per_ring[i]): 
                    x = radius*np.cos(j*2*np.pi/self.gran_per_ring[i])
                    y = .5*self.height
                    z = radius*np.sin(j*2*np.pi/self.gran_per_ring[i])
                    Interior(x,y,z,n,self.diameter,self.height,self.rho_bot,radius,self.material,self.interior,self.system,self.body_floor)
                    n += 1 # Move to the next ID for interior particles
                i += 1 # Move to the next index in self.gran_per_ring
            
            elif radius == 0: # Placing center particle
                x = 0
                y = .5*self.height
                z = 0
                Interior(x,y,z,n,self.diameter,self.height,self.rho_bot,radius,self.material,self.interior,self.system,self.body_floor)

        #-------------------------------------------
        #       Add a blue cylinder at the target
        #-------------------------------------------
        blue_color = chrono.ChColorAsset()
        blue_color.SetColor(chrono.ChColor(0,0,1))
        
        self.target = chrono.ChBodyEasyCylinder(self.diameter/4, self.height*2, self.rho_bot)
        self.target.SetPos(chrono.ChVectorD(self.X_targ, self.height, self.Z_targ))
        self.target.SetCollide(False)
        self.target.SetBodyFixed(True)
        self.target.AddAsset(blue_color)
        
        self.system.Add(self.target)
        
        #-------------------------
        #       Add an obstacle
        #-------------------------
        # self.obstacle = chrono.ChBodyEasyCylinder(self.R1*2, self.height, self.rho_bot)
        # x = self.X_targ/2
        # y = self.height*0.5
        # z = self.Z_targ/2
        # self.obstacle.SetPos(chrono.ChVectorD(x,y,z))
        # self.obstacle.SetMaterialSurface(self.material2)
        
        # self.obstacle.GetCollisionModel().ClearModel()
        # self.obstacle.GetCollisionModel().AddCylinder(self.R1*2, self.R1*2,self.height/2)
        # self.obstacle.GetCollisionModel().BuildModel()
        # self.obstacle.SetCollide(True)
        # self.obstacle.SetBodyFixed(True)
        
        # green_color=chrono.ChColorAsset()
        # green_color.SetColor(chrono.ChColor(0,1,0))
        # self.obstacle.AddAsset(green_color)
        
        # self.system.Add(self.obstacle)
        
        #---------------------------
        #       Add two circular obstacles
        #---------------------------
        green_color = chrono.ChColorAsset()
        green_color.SetColor(chrono.ChColor(0,1,0))
        
        self.obstacle_radius = self.R1*2
        self.gap = self.R1*1.40
        self.obstacle_x = self.X_targ/3
        self.obstacle_z = 0.5*self.gap + self.R1*2
        self.obstacle_y = self.height/2
        
        self.obstacle1 = chrono.ChBodyEasyCylinder(self.obstacle_radius, self.height, self.rho_bot)
        self.obstacle1.SetPos(chrono.ChVectorD(self.obstacle_x, self.obstacle_y, self.obstacle_z))
        self.obstacle1.SetMaterialSurface(self.material2)
        
        self.obstacle1.GetCollisionModel().ClearModel()
        self.obstacle1.GetCollisionModel().AddCylinder(self.R1*2, self.R1*2,self.height/2)
        self.obstacle1.GetCollisionModel().BuildModel()
        self.obstacle1.SetCollide(True)
        self.obstacle1.SetBodyFixed(True)
        
        self.obstacle1.AddAsset(green_color)
        self.obstacle1.SetId(self.num_bots+self.num_interior+1)
        self.obstacle_ids.append(self.num_bots+self.num_interior+1)
        self.system.Add(self.obstacle1)
        
        self.obstacle2 = chrono.ChBodyEasyCylinder(self.obstacle_radius, self.height, self.rho_bot)
        self.obstacle2.SetPos(chrono.ChVectorD(self.obstacle_x, self.obstacle_y, -self.obstacle_z))
        self.obstacle2.SetMaterialSurface(self.material2)
        
        self.obstacle2.GetCollisionModel().ClearModel()
        self.obstacle2.GetCollisionModel().AddCylinder(self.R1*2, self.R1*2,self.height/2)
        self.obstacle2.GetCollisionModel().BuildModel()
        self.obstacle2.SetCollide(True)
        self.obstacle2.SetBodyFixed(True)
        
        self.obstacle2.AddAsset(green_color)
        self.obstacle2.SetId(self.num_bots+self.num_interior+2)
        self.obstacle_ids.append(self.num_bots+self.num_interior+2)
        self.system.Add(self.obstacle2)
        
        #----------------------------------
        #       Add two walls with a hole
        #---------------------------------
        # wall_shape = chrono.ChBoxShape()
        # wall_shape.GetBoxGeometry().Size = chrono.ChVectorD(self.wall_thickness/2, self.wall_height/2, self.wall_width/2)
        # green_color=chrono.ChColorAsset()
        # green_color.SetColor(chrono.ChColor(0,1,0))
        
        # self.wall_1 = chrono.ChBody()
        # self.wall_1.SetBodyFixed(True)
        # self.wall_1.SetPos(chrono.ChVectorD(self.wall_x, self.wall_height/2, self.wall_z))
        # self.wall_1.SetMaterialSurface(self.material2)
        # self.wall_1.GetCollisionModel().ClearModel()
        # self.wall_1.GetCollisionModel().AddBox(self.wall_thickness/2, self.wall_height/2, self.wall_width/2)
        # self.wall_1.GetCollisionModel().BuildModel()
        # self.wall_1.SetCollide(True)
        
        # self.wall_1.AddAsset(wall_shape)
        # self.wall_1.AddAsset(green_color)
        
        # self.system.Add(self.wall_1)
        
        # self.wall_2 = chrono.ChBody()
        # self.wall_2.SetBodyFixed(True)
        # self.wall_2.SetPos(chrono.ChVectorD(self.wall_x, self.wall_height/2, -self.wall_z))
        # self.wall_2.SetMaterialSurface(self.material2)
        # self.wall_2.GetCollisionModel().ClearModel()
        # self.wall_2.GetCollisionModel().AddBox(self.wall_thickness/2, self.wall_height/2, self.wall_width/2)
        # self.wall_2.GetCollisionModel().BuildModel()
        # self.wall_2.SetCollide(True)
        
        # self.wall_2.AddAsset(wall_shape)
        # self.wall_2.AddAsset(green_color)
        
        # self.system.Add(self.wall_2)
        
        #-----------------------------------
        #     Get an observation to report
        #     Initialize steps    
        #-----------------------------------
        self.numsteps = 0
        self.step(np.zeros(self.action_size))
        
        ac=np.zeros(self.action_size)
        return self.get_ob(ac)

# In[Step Function]
    """
    Step Function - Take an action!
    """       
    def step(self, ac):
        self.numsteps += 1
        #-----------------------
        #       Perform action
        #-----------------------
        for i in range(len(self.forces)):
            self.forces[i].SetMforce(self.gain*ac[i]) # May need to change the shape of the action matrix ac
        #self.stiffSpring() # Stiffens the springs if they get too long
        
        self.system.DoStepDynamics(self.timestep)
        self.time += self.timestep
        obs = self.get_ob(ac)
        rew = self.calc_rew(ac)
                
        if self.data_collect:
            self.data_collection(ac, rew)
        
        return obs, rew, self.isdone, self.info

# In[Observation Function]    
    """
    Observation Function - What is our current state
    """         
    def get_ob(self, ac):
        
        bot_pos=[]
        bot_vel=[]
        bot_forces=[]
        bot_external_forces=np.zeros(20) # Initialize the forces as all 0
        in_pos=[]
        in_vel=[]
        
        # Observing the positions, velocities, and forces:
        for i in range(self.num_bots):
            # X-Direction Variables
            bot_pos.append(self.bots[i].GetPos().x)
            bot_vel.append(self.bots[i].GetPos_dt().x)
            bot_forces.append(self.bots[i].Get_Xforce().x)
            
            # Z-Direction Variables
            bot_pos.append(self.bots[i].GetPos().z)
            bot_vel.append(self.bots[i].GetPos_dt().z)
            bot_forces.append(self.bots[i].Get_Xforce().z)

            self.X_Pos.append(self.bots[i].GetPos().x) # Stores all x-pos of the bots for power consumption processing
            self.Z_Pos.append(self.bots[i].GetPos().z) # Stores all z-pos of the bots for power consumption processing 
        
        # Observing external forces on the bots:
        self.my_rep.ResetList()
        self.system.GetContactContainer().ReportAllContacts(self.my_rep)
        contact_list = self.my_rep.GetList()
        
        colliding_objects = contact_list[6]
        x_collision_forces = contact_list[3]
        z_collision_forces = contact_list[5]
        
        collision_number = 0 # Use this to iterate through the collisions
        for collision in colliding_objects:
            # Each collision gives a collision pair. 
            # For example, [1,2] means objects with ID 1 and 2 are colliding.
            for bot_id in range(1,self.num_bots+1):
                if bot_id in collision:  # Check if the collision is with a bot
                    # Need to make sure the collision is with an external object                    
                    for obstacle in self.obstacle_ids:
                        if obstacle in collision:
                            # NOW we can add the forces!
                            bot_external_forces[bot_id*2-2] += x_collision_forces[collision_number]
                            bot_external_forces[bot_id*2-1] += z_collision_forces[collision_number]
            collision_number += 1
        
        for particle in range(self.num_interior):
            in_pos.append(self.interior[particle].GetPos().x)
            in_vel.append(self.interior[particle].GetPos_dt().x)
            
            in_pos.append(self.interior[particle].GetPos().z)
            in_pos.append(self.interior[particle].GetPos_dt().z)
            
        
        #Put it all together now!
        obs = np.concatenate((bot_pos, bot_vel, bot_forces, bot_external_forces, in_pos, in_vel))
        # obs_norm = np.linalg.norm(obs)
        # obs = obs/obs_norm
        return obs

# In[Reward function]
    """
    Reward Function - How good is this?
    """
    def calc_rew(self, ac):
        
        progress = self.calc_progress()
        power_used = self.power_consumption()
        power_weight = -2.0 # Hyperparameter
        #collide = self.collide()
        forces_used = self.force_use(ac)
        
        rew = progress #- forces_used#+ power_weight*power_used
        rew = self.is_done(rew) #Changes the reward if we reach a terminal state. If not, no worries!

        return rew

# In[Different functions for calculating reward]
    """
    Progress function - How close are we to the target
    """
    def calc_progress(self):
        
        # Find the center of JAMoEBA
        x_center = 0
        z_center = 0
        for i in range(self.num_bots):
            x_center += self.bots[i].GetPos().x
            z_center += self.bots[i].GetPos().z
        x_center /= self.num_bots
        z_center /= self.num_bots
        
        d = np.linalg.norm([self.X_targ - x_center,self.Z_targ - z_center])
        progress = -(d - self.d_old) / self.timestep
        self.d_old = d
        
        return progress
    
    """
    Collide function - Don't touch the wall!'
    """
    def collide(self):
        
         wall_1_touch = self.wall_1.GetContactForce().Length2()
         wall_2_touch = self.wall_2.GetContactForce().Length2()
         touch = wall_1_touch + wall_2_touch
         
         if touch > 0:
             return -1
             
         else:
             return 0
        
    """
    Force Use - try not to use too many actions! Will take the current actions taken and penalize.
    """
    def force_use(self, ac):
        for action in ac:
            self.actions_taken += abs(action)
            
        forces_used = self.actions_taken/self.total_force_needed #penalizes according to the total forces used divded by the total forces available.
        return forces_used
    
    """
    Power Consumption - How much energy was used?
    """
    def power_consumption(self):
        # Multiples F*dx
        
        power_x=[]
        power_z=[]
        
        for i in range(self.num_bots):
            self.X_Pos.append(self.bots[i].GetPos().x)
            self.Z_Pos.append(self.bots[i].GetPos().z)
        
        for i in range(self.num_bots): # Proud of myself for thinking of this one - EL
            dx = self.X_Pos[-self.num_bots + i] - self.X_Pos[(-self.num_bots - self.num_bots) + i]
            dz = self.Z_Pos[-self.num_bots + i] - self.Z_Pos[(-self.num_bots - self.num_bots) + i]
            
            power_x.append(self.forces[i].GetMforce() * dx) # Use the previous action * gain to get the force applied!
            power_z.append(self.forces[i + 1].GetMforce() * dz) # Use the previous action * gain to get the force applied!
        
        power_used = sum(power_x) + sum(power_z)
        return power_used
    
# In[Spring Stiffener]
    """
    If the springs are stretched too much, then they are stiffened
    """
    # In[Set springs]   
    def stiffSpring(self):
        for spring in self.Springs:
            spring_length = spring.Get_SpringLength()
        
        if spring_length < self.spring_max:
            spring.Set_SpringK(self.k)
    
        elif spring_length >= self.spring_max:
            spring.Set_SpringK(self.k_stiff)


# In[Is_done function]
    """
    Done function - Kills the episode if it takes too long
    """       
    def is_done(self, rew):
        
        x_center = 0
        z_center = 0
        for i in range(self.num_bots):
            x_center += self.bots[i].GetPos().x
            z_center += self.bots[i].GetPos().z
        x_center /= self.num_bots
        z_center /= self.num_bots
        
        if (self.numsteps*self.timestep>500 or abs(x_center)>=self.X_targ+1.0 or abs(z_center)>=self.Z_targ+1.0):
            self.isdone = True
            rew -= 1 # Penalizing going off track or takes too long to get there.
            return rew
            
        elif (x_center >=self.X_targ-.01 and x_center<=self.X_targ+.01 and z_center>=self.Z_targ-.01 and z_center<=self.Z_targ+.01):
            self.isdone = True
            rew += 100 # Add an extra 100 points for reaching the target
            return rew
        
        else:
            return rew # Does not change the reward
            
# In[Render Function]            
    def render(self):
        if not self.render_setup :
            if self.POV_Ray: # Creates POV_Ray Application
                chrono.SetChronoDataPath("C:/Chrono/Chrono_dependencies/Chrono_develop/data/")
                self.script_dir = os.path.dirname('povvideofiles' + self.experiment_name + '/')
                self.pov_exporter = postprocess.ChPovRay(self.system)
                
                # Sets some file names for in-out processes
                self.pov_exporter.SetTemplateFile(chrono.GetChronoDataPath() + "_template_POV.pov") # This line may be redundant according to Chrono's website
                self.pov_exporter.SetOutputScriptFile("rendering_" + self.experiment_name + ".pov")
                self.pov_exporter.SetOutputDataFilebase("my_state")
                self.pov_exporter.SetPictureFilebase("picture")

                # Creates folders
                self.POV_output = self.experiment_name+"_output"
                self.POV_anim = self.experiment_name+"_anim"
                if not os.path.exists(self.POV_output):
                    os.mkdir(self.POV_output)
                if not os.path.exists(self.POV_anim):
                    os.mkdir(self.POV_anim)
                    
                self.pov_exporter.SetOutputDataFilebase(self.POV_output + "/my_state")
                self.pov_exporter.SetPictureFilebase(self.POV_anim + "/picture")
                self.pov_exporter.SetCamera(chrono.ChVectorD(self.X_targ/2,2,self.Z_targ/2), # Camera Location
                                            chrono.ChVectorD(self.X_targ/2,0,self.Z_targ/2),# Camera Point
                                            90) #Camera Angle
                
                self.pov_exporter.AddAll()
                self.pov_exporter.ExportScript()
                self.count = 0
                self.render_setup = True
                
                
            else: #Creates Irrlicht Application
                self.myapplication = chronoirr.ChIrrApp(self.system, self.experiment_name, chronoirr.dimension2du(1024,768))
                self.myapplication.AddShadowAll()
                self.myapplication.SetTimestep(self.timestep)
                self.myapplication.AddTypicalSky(chrono.GetChronoDataPath() + '/skybox/')
                self.myapplication.AddTypicalCamera(chronoirr.vector3df(self.X_targ/2,2,self.Z_targ/2), # Camera Placement
                                                    chronoirr.vector3df(self.X_targ,0,self.Z_targ)) # Camera Point
                self.myapplication.AddLightWithShadow(chronoirr.vector3df(-.5,2,0),    # point
                                                      chronoirr.vector3df(0,0,0),    # aimpoint
                                                      20,                 # radius (power)
                                                      1,10,               # near, far
                                                      120)                # angle of FOV
                self.myapplication.AssetBindAll()
                self.myapplication.AssetUpdateAll()
                self.render_setup = True
                
        if self.POV_Ray:
            self.pov_exporter.SetCamera(chrono.ChVectorD(self.X_targ/2,2,self.Z_targ/2), # Camera Location
                                        chrono.ChVectorD(self.X_targ/2, 0, self.Z_targ/2),# Camera Point
                                        90) #Camera Angle
            self.pov_exporter.AddAll()
            self.pov_exporter.ExportScript()
            self.count+=1
            if self.count%12 == 0: # Exports every 12th frame
                self.pov_exporter.ExportData()
            
        else:
            self.myapplication.GetDevice().run()
            self.myapplication.BeginScene()
            self.myapplication.DrawAll()
            #self.myapplication.DoStep() # This might need to be uncommented
            self.myapplication.EndScene()

# In[Close function]
    """
    Close Function - Ensures the simulation is closed
    """        
    def close(self): #Changed the title of this to close
        if self.render_setup:
            if self.POV_Ray:
                None
                
            else:
                self.myapplication.GetDevice().closeDevice()
                print('Destructor called, Device deleted.')
        else:
            print('Destructor called, No device to delete.')
            
# In[Data Collection Function]
    def data_collection(self, ac, rew):
        
        # Create new and empty vectors 
        X_Pos_temp = [self.time]
        Y_Pos_temp = [self.time]
        Z_Pos_temp = [self.time]
        
        X_vel_temp = [self.time]
        Y_vel_temp = [self.time]
        Z_vel_temp = [self.time]
        
        forces_temp = [self.time]
        
        action_temp = [self.time]
        rew_temp = [self.time]
        
        # Append to the temporary lists
        for i in range(self.num_bots):
            X_Pos_temp.append(self.bots[i].GetPos().x)
            Y_Pos_temp.append(self.bots[i].GetPos().y)
            Z_Pos_temp.append(self.bots[i].GetPos().z)
            
            X_vel_temp.append(self.bots[i].GetPos_dt().x)
            Y_vel_temp.append(self.bots[i].GetPos_dt().y)
            Z_vel_temp.append(self.bots[i].GetPos_dt().z)
            
            forces_temp.append(self.bots[i].Get_Xforce().x)
            forces_temp.append(self.bots[i].Get_Xforce().z)
            
        for i in range(len(ac)):
            action_temp.append(ac[i])
        
        # Convert to Numpy Arrays
        rew_temp.append(rew)
        
        X_Pos_temp = np.asarray(X_Pos_temp)
        Y_Pos_temp = np.asarray(Y_Pos_temp)
        Z_Pos_temp = np.asarray(Z_Pos_temp)
        
        X_vel_temp = np.asarray(X_vel_temp)
        Y_vel_temp = np.asarray(Y_vel_temp)
        Z_vel_temp = np.asarray(Z_vel_temp)
        
        forces_temp = np.asarray(forces_temp)
        
        action_temp = np.asarray(action_temp)
        rew_temp = np.asarray(rew_temp)
        
        # Now append to the master list
        self.X_data = np.vstack([self.X_data, X_Pos_temp])
        self.X_vel_data = np.vstack([self.X_vel_data, X_vel_temp])
        self.Y_data = np.vstack([self.Y_data, Y_Pos_temp])
        self.Y_vel_data = np.vstack([self.Y_vel_data, Y_vel_temp])
        self.Z_data = np.vstack([self.Z_data, Z_Pos_temp])
        self.Z_vel_data = np.vstack([self.Z_vel_data, Z_vel_temp])
        self.force_data = np.vstack([self.force_data, forces_temp])
        self.ac = np.vstack([self.ac, action_temp])
        self.reward_data = np.vstack([self.reward_data, rew_temp])
        
        
# In[Date Exportation]
        
    def data_export(self):
        
        # Save the parameters of this experiment:
        txt_file= self.new_folder + 'TESTING_Environment_paramters.txt' 
        
        with open(txt_file, 'w') as f:
            for line in self.environment_parameters:
                f.write("%s\n" % line)
        
        np.savetxt(self.new_folder + 'X_data.csv', self.X_data, delimiter=',')
        np.savetxt(self.new_folder + 'X_vel_data.csv', self.X_vel_data, delimiter=',')
        np.savetxt(self.new_folder + 'Y_data.csv', self.Y_data, delimiter=',')
        np.savetxt(self.new_folder + 'Y_vel_data.csv', self.Y_vel_data, delimiter=',')
        np.savetxt(self.new_folder + 'Z_data.csv', self.Z_data, delimiter=',')
        np.savetxt(self.new_folder + 'Z_vel_data.csv', self.Z_vel_data, delimiter=',')
        np.savetxt(self.new_folder + 'force_data.csv', self.force_data, delimiter=',')
        np.savetxt(self.new_folder + 'actions.csv', self.ac, delimiter=',')
        np.savetxt(self.new_folder + 'reward.csv', self.reward_data, delimiter=',')
        
        if self.plot: # Plot the data now!!
            self.plot_data()
        
# In[Save Parameters from Training]
        
    def parameter_export(self):
        parameter_file=  self.experiment_name + ' TRAINING_environment_parameters.txt'
        
        with open(parameter_file, 'w') as f:
            for line in self.environment_parameters:
                f.write("%s\n" % line)

# In[Data Plotter]
                
    def plot_data(self):
        
        # Common Among Position and Velocity Data
        last_col = len(self.X_data[0])-1
        time = self.X_data[:,0]
        xlabel = 'Time [sec]'
        
        # Plot X-Position
        X_COM = []
        for row in self.X_data:
            pos = mean(row[1:last_col])
            X_COM.append(pos)
        plt.figure('X-Pos')
        plt.plot(time,X_COM)
        plt.xlabel(xlabel)
        plt.ylabel('X-Position [m]')
        plt.title('X-Center Position')
        plt.savefig(self.new_folder + 'X-Center Position.jpg')       
        
        # Plot Y-Position
        Y_COM = []
        for row in self.Y_data:
            pos = mean(row[1:last_col])
            Y_COM.append(pos)
        plt.figure('Y-Pos')
        plt.plot(time,Y_COM)
        plt.xlabel(xlabel)
        plt.ylabel('Y-Position [m]')
        plt.title('Y-Center Position')
        plt.savefig(self.new_folder + 'Y-Center Position.jpg')
        
        # Plot Z-Position
        Z_COM = []
        for row in self.Z_data:
            pos = mean(row[1:last_col])
            Z_COM.append(pos)
        plt.figure('Z-Pos')
        plt.plot(time,Z_COM)
        plt.xlabel(xlabel)
        plt.ylabel('Z-Position [m]')
        plt.title('Z-Center Position')
        plt.savefig(self.new_folder + 'Z-Center Position.jpg')
        
        # Plot X-velocity
        plt.figure('X-Vel')
        for i in range(self.num_bots):
            plt.plot(time, self.X_vel_data[:,i+1], label = 'Bot' + str(i+1))
        plt.xlabel(xlabel)
        plt.ylabel('X Velocity [m/s]')
        plt.title('X-Velocity')
        plt.legend(loc='lower right')
        plt.savefig(self.new_folder + 'X-Velocity.jpg')
        
        # Plot Y-Velocity
        plt.figure('Y-Vel')
        for i in range(self.num_bots):
            plt.plot(time, self.Y_vel_data[:,i+1], label = 'Bot' + str(i+1))
        plt.xlabel(xlabel)
        plt.ylabel('Y Velocity [m/s]')
        plt.title('Y-Velocity')
        plt.legend(loc='lower right')
        plt.savefig(self.new_folder + 'Y-Velocity.jpg')
        
        # Plot Z-Velocity
        plt.figure('Z-Vel')
        for i in range(self.num_bots):
            plt.plot(time, self.Z_vel_data[:,i+1], label = 'Bot' + str(i+1))
        plt.xlabel(xlabel)
        plt.ylabel('Z Velocity [m/s]')
        plt.title('Z-Velocity')
        plt.legend(loc='lower right')
        plt.savefig(self.new_folder + 'Z-Velocity.jpg')
        
        
        # Plot forces
        last_col_2 = len(self.force_data[0])-1
        bot=1
        for i in range(last_col_2):
            if i%2!=0:
                plt.figure('Forces Bot ' + str(bot))
                plt.plot(time, self.force_data[:,i], label='X-Force')
                plt.plot(time, self.force_data[:,i+1], label='Z-Force')
                plt.xlabel(xlabel)
                plt.ylabel('Force [N]')
                plt.title('Forces on Bot ' + str(bot))
                plt.legend(loc='lower right')
                plt.savefig(self.new_folder + 'Bot ' + str(bot) + ' Forces.jpg')
                
                bot+=1
                
        # Plot reward
        time = self.reward_data[:,0]
        rewards = self.reward_data[:,1]
        plt.figure('Rewards')
        plt.plot(time, rewards)
        plt.xlabel(xlabel)
        plt.ylabel('Reward')
        plt.title('Reward for JAMoEBA')
        plt.savefig(self.new_folder + 'Reward.jpg')
            