# -*- coding: utf-8 -*-
"""
Created on Thu Mar  5 22:20:55 2020

@author: elopez8
"""
import Strings_Environment as Strings
import os

from stable_baselines.common.policies import FeedForwardPolicy, register_policy
from stable_baselines import PPO1

experiment_name = 'Experiment_45'

tensorboard_log = experiment_name + " Tensorboard Log/"
if not os.path.exists(tensorboard_log):
    os.makedirs(tensorboard_log)

n=400 # Number of nodes in a hidden layer

# Custom MLP policy of three layers of size 'n' each
class CustomPolicy(FeedForwardPolicy):
    def __init__(self, *args, **kwargs):
        super(CustomPolicy, self).__init__(*args, **kwargs,
                                           net_arch=[dict(pi=[n, n],
                                                          vf=[n, n])],
                                           feature_extraction="mlp")

# Register the policy, it will check that the name is not already taken
register_policy('CustomPolicy', CustomPolicy)

env = Strings.Strings(experiment_name=experiment_name)
model = PPO1('CustomPolicy', env, verbose=1, tensorboard_log=tensorboard_log)
model.learn(total_timesteps=1000000)
model.save('ppo1_Strings_CustomPolicy_' + experiment_name)
env.parameter_export()
env.close()


